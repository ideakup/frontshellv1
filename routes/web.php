<?php
$devmode=env('APP_DEV');

$activeLangCount = App\Language::where('deleted', 'no')->where('status', 'active')->count();
$defaultLang = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
$langs = App\Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
//dd($activeLangCount);



$mainMenu = App\Menu::where(
	function ($query){
		$query->where('position', 'all')->orWhere('position', 'top');
	}
)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

//

if(count(Request::segments()) == 0){
	if ($activeLangCount == 1) {
		
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: /'.$mainMenu->variable->slug);
		//exit();
	}else{
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: /'.$defaultLang->code.'/'.$mainMenu->variable->slug);
		//exit();
	}

}

else{

	$firstsegment=Request::segments()[0];
	if ($activeLangCount == 1) {

		if($devmode){
			$langcheck = App\MenuVariable::where('lang_code',$firstsegment)->count();
	//dd($slugcheck);
			if($langcheck!=0){
				$secondsegment=Request::segments()[1];
				$slugcheck = App\MenuVariable::where('slug',$secondsegment)->count();
				if($slugcheck!=0){
					$url=Request::segments();

					$deleted_url = array_shift($url);
					$newurl = implode("/", $url);
				//echo "$newurl";
					header('HTTP/1.1 301 Moved Permanently');
					header('Location:/'.$newurl);
					exit();
				}
				else{
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /'.$mainMenu->variable->slug);
					exit();

				}
			}
			else{
				$slugcheck = App\MenuVariable::where('slug',$firstsegment)->count();
				if($slugcheck==0){
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /'.$mainMenu->variable->slug);
					exit();
				}

			}

		}

		Route::get('/{slug}/{attr?}/{param?}/{param2?}', 'HomeController@indexOneLang');




	}else{
		if($devmode){
			$slugcheck = App\MenuVariable::where('slug',$firstsegment)->count();

			if($slugcheck!=0){

				$sluglang= App\MenuVariable::where('slug',$firstsegment)->first();
				$url=Request::segments();
				$newurl = implode("/", $url);
				$newurl=$sluglang->lang_code."/".$newurl;

				header('HTTP/1.1 301 Moved Permanently');
				header('Location:/'.$newurl);
				exit();

			}
			else{
				$langcheck = App\MenuVariable::where('lang_code',$firstsegment)->count();

				if($langcheck==0){
					header('HTTP/1.1 301 Moved Permanently');
					header('Location: /'.$defaultLang->code.'/'.$mainMenu->variable->slug);
					exit();
				}
				else{
					$secondsegment=Request::segments()[1];
					$slugcheck = App\MenuVariable::where('slug',$secondsegment)->count();
					if($slugcheck==0){
						header('HTTP/1.1 301 Moved Permanently');
						header('Location: /'.$defaultLang->code.'/'.$mainMenu->variable->slug);
						exit();
					}
				}

			}

		}



		Route::get('/{lang}/{slug}/{attr?}/{param?}/{param2?}', 'HomeController@index');



	}

	Route::post('/{lang}/form_save', 'HomeController@form_save');

}