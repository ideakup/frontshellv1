let mix = require('laravel-mix');

mix.copy('resources/assets/images', 'public/images');
mix.copy('resources/assets/favicons', 'public/favicons');
mix.copy('resources/assets/custom/favicons', 'public/favicons');
mix.copy('resources/assets/custom/logos', 'public/logos');
mix.copy('resources/assets/custom/cimages', 'public/cimages');
mix.copy('resources/assets/custom_modules/amcharts', 'public/amcharts');
mix.copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/webfonts');
mix.copy('resources/assets/theme/css/fonts', 'public/css/fonts');
mix.copy('resources/assets/theme/images', 'public/images');

mix.combine([
	'resources/assets/theme/js/jquery.js',
	'resources/assets/theme/js/plugins.js',
	'resources/assets/theme/js/functions.js',
	'resources/assets/theme/js/jquery.gmap.js',
	//'node_modules/jquery/dist/jquery.js',
    //'node_modules/bootstrap/dist/js/bootstrap.bundle.js',
    'node_modules/jquery-match-height/dist/jquery.matchHeight-min.js',
    'node_modules/@fortawesome/fontawesome-free/js/all.js',
    //'node_modules/lightbox2/dist/js/lightbox.min.js',
	'node_modules/formBuilder/dist/form-render.min.js',
	'node_modules/select2/dist/js/select2.min.js',
], 'public/js/app.js').version();

mix.combine([
    'resources/assets/custom_modules/amcharts4/dist/script/core.js',
	'resources/assets/custom_modules/amcharts4/dist/script/maps.js',
	'resources/assets/custom_modules/amcharts4/dist/geodata/script/turkeyLow.js',
	'resources/assets/custom_modules/amcharts4/dist/script/themes/animated.js',
], 'public/js/ammapcustom.js').version();

mix.combine([
	'resources/assets/theme/css/bootstrap.css',
	'resources/assets/theme/style.css',
	'resources/assets/theme/css/swiper.css',
	'resources/assets/theme/css/dark.css',
	'resources/assets/theme/css/font-icons.css',
	'resources/assets/theme/css/animate.css',
	'resources/assets/theme/css/magnific-popup.css',
	'resources/assets/theme/css/responsive.css',
    //'node_modules/bootstrap/dist/css/bootstrap.css',
	'node_modules/@fortawesome/fontawesome-free/css/all.css',
	//'node_modules/lightbox2/dist/css/lightbox.min.css',
	'node_modules/select2/dist/css/select2.min.css',
	'resources/assets/custom/style.css'
], 'public/css/app.css').version();
