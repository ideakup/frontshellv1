<?php

namespace App\Http\Controllers;

use Auth;
use Redirect;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\User;
use App\Menu;
use App\MenuVariable;
use App\Content;
use App\ContentVariable;
use App\SiteSettings;
use App\Tag;
use App\TagVariable;
use App\Category;
use App\CategoryVariable;
use App\Language;

use App\Calendar;
use App\CalendarVariable;

use App\FormData;

use App\TopHasSub;
use App\ContentHasTag;
use App\ContentHasCategory;

use Illuminate\Support\Arr;
use FeedReader;

use Mail;
use App\Mail\FormSendMail;


class HomeController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index($lang, $slug, $attr = null, $param = null, $param2 = null)
    {
        //DB::enableQueryLog(); //dd(DB::getQueryLog());
        //dd($lang);
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $langSlug = $lang;

        $menu = MenuVariable::where('slug', $slug)->first()->menu;
        $headerslug = $menu;
        while ($headerslug->top_id != null) {
            $headerslug = $headerslug->topMenu;
        }
        $headerslug = $headerslug->variables->where('lang_code', $lang)->first()->slug;

        $menuall = Menu::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $mainmenu = Menu::where(
            function ($query){
                $query->where('position', 'all')->orWhere('position', 'top');
            }
        )->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

        $topmenus = Menu::where('position', '!=', 'aside')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $asidemenus = Menu::where('position', '!=', 'top')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $sitesettings = SiteSettings::all();
        $startDate = Carbon::now()->copy()->startOfDay()->subMonths(3);

        $wsConfig = config('webshell.pageProperties.default');
        $webshellIndex = '';
        if (is_null($attr)) { $webshellIndex = $slug; }else{ $webshellIndex = $attr; }
        if (!is_null(config('webshell.pageProperties.'.$webshellIndex))) {
            foreach (config('webshell.pageProperties.'.$webshellIndex) as $key => $value) {
                $wsConfig[$key] = $value;
            }
        }

        return view('home', array(
            'lang' => $lang,
            'langSlug' => $langSlug,
            'slug' => $slug, 
            'attr' => $attr, 
            'param' => $param,
            'param2' => $param2,
            
            'langs' => $langs, 
            'menu' => $menu, 
            'menuall' => $menuall, 
            'mainmenu' => $mainmenu, 
            'topmenus' => $topmenus, 
            'asidemenus' => $asidemenus, 
            'sitesettings' => $sitesettings,

            'headerslug' => $headerslug, 
            'wsConfig' => $wsConfig
        ));
    }

    public function indexOneLang($slug, $attr = null, $param = null, $param2 = null)
    {
        //DB::enableQueryLog(); //dd(DB::getQueryLog());
        $lang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first()->code;
        $langs = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $langSlug = '';
        
        $menu = MenuVariable::where('slug', $slug)->first()->menu;
        $headerslug = $menu;
        while ($headerslug->top_id != null) {
            $headerslug = $headerslug->topMenu;
        }
        $headerslug = $headerslug->variables->where('lang_code', $lang)->first()->slug;

        $menuall = Menu::where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $mainmenu = Menu::where(
            function ($query){
                $query->where('position', 'all')->orWhere('position', 'top');
            }
        )->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();

        $topmenus = Menu::where('position', '!=', 'aside')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();
        $asidemenus = Menu::where('position', '!=', 'top')->where('top_id', null)->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->get();

        $sitesettings = SiteSettings::all();
        $startDate = Carbon::now()->copy()->startOfDay()->subMonths(3);

        $wsConfig = config('webshell.pageProperties.default');
        $webshellIndex = '';
        if (is_null($attr)) { $webshellIndex = $slug; }else{ $webshellIndex = $attr; }
        if (!is_null(config('webshell.pageProperties.'.$webshellIndex))) {
            foreach (config('webshell.pageProperties.'.$webshellIndex) as $key => $value) {
                $wsConfig[$key] = $value;
            }
        }
        
        return view('home', array(
            'lang' => $lang,
            'langSlug' => $langSlug,
            'slug' => $slug, 
            'attr' => $attr, 
            'param' => $param,
            'param2' => $param2,
            
            'langs' => $langs, 
            'menu' => $menu, 
            'menuall' => $menuall, 
            'mainmenu' => $mainmenu, 
            'topmenus' => $topmenus, 
            'asidemenus' => $asidemenus, 
            'sitesettings' => $sitesettings, 
            
            'headerslug' => $headerslug,
            'wsConfig' => $wsConfig
        ));
    }


    public function form_save(Request $request)
    {

        $jsonEncodeReq = json_encode($request->input());
        $jsonDecodeReq = json_decode($jsonEncodeReq, true);
        Arr::pull($jsonDecodeReq, '_token');
        Arr::pull($jsonDecodeReq, 'lang');
        Arr::pull($jsonDecodeReq, 'source_type');
        Arr::pull($jsonDecodeReq, 'source_id');
        Arr::pull($jsonDecodeReq, 'form_id');

        $message = array(
            'success'           => 'Başarıyla Kaydedildi. Teşekkür Ederiz.',
            'error'             => 'Bir Hata Oluştu.',//'Email <strong>could not</strong> be sent due to some Unexpected Error. Please Try Again later.',
            'error_bot'         => 'Bot Detected! Form could not be processed! Please Try Again!',
            'error_unexpected'  => 'An <strong>unexpected error</strong> occured. Please Try Again later.',
            'recaptcha_invalid' => 'Captcha not Validated! Please Try Again!',
            'recaptcha_error'   => 'Captcha not Submitted! Please Try Again.'
        );

        $formdata = new FormData();
        $formdata->lang_code = $request->lang;
        $formdata->source_type = $request->source_type;
        $formdata->source_id = $request->source_id;
        $formdata->form_id = $request->form_id;
        $formdata->data = json_encode($jsonDecodeReq);
        $formdata->save();

        if($request->source_type == 'menu'){
            $ths = TopHasSub::where('top_menu_id', $request->source_id)->where('sub_content_id', $request->form_id)->first();
            $content = Content::where('id', $request->form_id)->first();
            $contentVar = $content->variableLang($request->lang)->content;
        }

        if(empty(json_decode($ths->props)->props_eposta)){
            echo '{ "alert": "success", "message": "' . $message['success'] . '" }';

        }
        else{
            Mail::to([json_decode($ths->props)->props_eposta])->send(new FormSendMail($formdata, $contentVar));

            echo '{ "alert": "success", "message": "' . $message['success'] . '" }';
        //return back()->with('status', 'Profile updated!');

        }
        

    }

}
