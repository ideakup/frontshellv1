<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopHasSub extends Model
{
    protected $table = 'top_has_sub';

    public function topMenu()
    {
        return $this->hasOne('App\Menu', 'id', 'top_menu_id');
    }

    public function topContent()
    {
        return $this->hasOne('App\Content', 'id', 'top_content_id');
    }

    public function subMenu()
    {
        return $this->hasOne('App\Menu', 'id', 'sub_menu_id');
    }
    
    public function subContent()
    {
        return $this->hasOne('App\Content', 'id', 'sub_content_id');
    }
    
}
