<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Menu extends Model
{
    protected $table = 'menu';

    /*
        public function content()
        {
            return $this->belongsToMany('App\Content', 'menu_has_content')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
        }
    
        public function content()
        {
            return $this->belongsToMany('App\Content', 'top_has_sub', 'top_menu_id', 'sub_content_id')->where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc');
        }

        public function menuHasContent()
        {
            return $this->hasMany('App\MenuHasContent', 'menu_id', 'id')->orderBy('order', 'asc');
        }
    */

    public function topHasSub()
    {
        return $this->hasMany('App\TopHasSub', 'top_menu_id', 'id')->where('top_content_id', NULL)->orderBy('order', 'asc');
    }

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\MenuVariable', 'menu_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\MenuVariable', 'menu_id', 'id');
    }

    public function topMenu()
    {
        return $this->hasOne('App\Menu', 'id', 'top_id')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }

    public function subMenu()
    {
        return $this->hasMany('App\Menu', 'top_id', 'id')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }

    public function subMenuTop()
    {
        return $this->hasMany('App\Menu', 'top_id', 'id')->where('position', '!=', 'aside')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }

    public function subMenuAside()
    {
        return $this->hasMany('App\Menu', 'top_id', 'id')->where('position', '!=', 'top')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
    
    public function slider()
    {
        return $this->hasMany('App\Slider', 'menu_id', 'id')->where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc');
    }
}
