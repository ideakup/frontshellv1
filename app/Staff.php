<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'map_staff';

    public function variable()
    {
        $currentLang = Language::where('deleted', 'no')->where('status', 'active')->orderBy('order', 'asc')->first();
        return $this->hasOne('App\StaffVariable', 'staff_id', 'id')->where('lang_code', $currentLang->code);
    }

    public function variableLang($langcode)
    {
        return $this->hasOne('App\StaffVariable', 'staff_id', 'id')->where('lang_code', $langcode)->first();
    }

    public function variables()
    {
        return $this->hasMany('App\StaffVariable', 'staff_id', 'id');
    }

    public function district()
    {
        return $this->hasOne('App\District', 'id', 'district_id');
    }

    public function city()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function county()
    {
        return $this->hasOne('App\County', 'id', 'county_id');
    }

}
