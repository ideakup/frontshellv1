<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagVariable extends Model
{
    protected $table = 'tagvariable';

    public function tag()
    {
        return $this->belongsTo('App\Tag', 'tag_id', 'id');
    }
}
