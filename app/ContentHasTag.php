<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentHasTag extends Model
{
    protected $table = 'content_has_tag';

    public function content()
    {
        return $this->belongsTo('App\Content', 'content_id', 'id');
    }
}
