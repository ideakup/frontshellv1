<!DOCTYPE html>
<html dir="ltr" lang="{{ app()->getLocale() }}">
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
  header("Cache-Control: max-age=3600"); //HTTP 1.1


  ?>

  <meta name="title" content="{{ $sitesettings->where('slug', 'seo-title')->first()->value }}"> <!-- Max 70 char -->
  <meta name="description" content="{{ $sitesettings->where('slug', 'seo-description')->first()->value }}"> <!-- Max 150 char -->
  <meta name="keywords" content="{{ $sitesettings->where('slug', 'seo-keyword')->first()->value }}">

  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="language" content="Turkish">
  <meta name="revisit-after" content="10 days">
  <meta name="author" content="İdeaküp Web Teknolojileri Ltd. Şti.">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $sitesettings->where('slug', 'seo-title')->first()->value }} {{ $menu->variableLang($lang)->menutitle }}</title>

  <link rel="apple-touch-icon" sizes="57x57" href="{{ url('favicons/apple-icon-57x57.png') }}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{ url('favicons/apple-icon-60x60.png') }}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{ url('favicons/apple-icon-72x72.png') }}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ url('favicons/apple-icon-76x76.png') }}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{ url('favicons/apple-icon-114x114.png') }}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ url('favicons/apple-icon-120x120.png') }}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{ url('favicons/apple-icon-144x144.png') }}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ url('favicons/apple-icon-152x152.png') }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{ url('favicons/apple-icon-180x180.png') }}">
  <link rel="icon" type="image/png" sizes="192x192" href="{{ url('favicons/android-icon-192x192.png') }}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{ url('favicons/favicon-32x32.png') }}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{ url('favicons/favicon-96x96.png') }}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{ url('favicons/favicon-16x16.png') }}">
  <link rel="manifest" href="{{ url('favicons/manifest.json') }}">

  <link href="https://fonts.googleapis.com/css2?{{config('webshell.siteDefaults.fontFamily')}}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/icofont/icofont.min.css') }}">

  <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>

<body class="stretched {{ $slug }} {{ $wsConfig['bodyClass'] }} no-transition">

    @php
    $__headertheme = $menu->headertheme;
    $__slidertype = $menu->slidertype;
    $__breadcrumbvisible = $menu->breadcrumbvisible;

    $segments = '';
    for ($i=1; $i < count(Request::segments()); $i++) { 
        $segments = $segments.'/'.Request::segments()[$i];
    }
    @endphp

    <div id="wrapper" class="clearfix">

        @include('partials.header')
        @include('partials.slidertype')

        @yield('content')

        @include('partials.footer'.config('webshell.siteDefaults.footerType'))

    </div>
    
    <script src="{{ mix('js/app.js') }}"></script>
    <script src="{{-- mix('js/ammapcustom.js') --}}"></script>

    @yield('inline-scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4dLiSF8_8s_IpcNQaguGO3cxglgqcvxc&callback=initMap" async defer></script>

    <script>
        $(function() {
            $('.entry-list-continer>.entry-title').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
            $('.entry-list-continer>.entry-content').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        });
    </script>

    {!! $sitesettings->where('slug', 'google-analytics-code')->first()->value !!}
</body>
</html>
