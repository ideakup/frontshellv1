@php
  // dump(json_decode($ths->props)->props_sum_type);
   //dd(json_decode($ths->props)->props_sum_count);

@endphp

<div class="{{ json_decode($ths->props)->props_sum_colvalue}}">


    <div class="container clearfix">


        <div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="{{ json_decode($ths->props)->props_sum_count }}" data-items-lg="{{ json_decode($ths->props)->props_sum_count }}" data-items-xl="{{ json_decode($ths->props)->props_sum_count }}">

           @foreach ($menuSum->topHasSub as $ths)

           @php
           $content = $ths->subContent;
           if (empty($content->variableLang($lang))) {
            $contVariable = $content->variable;
        }else{
            $contVariable = $content->variableLang($lang);
        }
        @endphp

        <div class="oc-item">
            <div class="ipost clearfix">
                <div class="entry-image">
                    <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                        @php $isAvailable = false; @endphp
                        @foreach ($content->subContentThs as $cths)
                        @if($cths->subContent->type == 'photo')
                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content) }}" />
                        @php $isAvailable = true; @endphp
                        @break
                        @elseif($cths->subContent->type == 'photogallery')
                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
                        @php $isAvailable = true; @endphp
                        @break
                        @endif
                        @endforeach
                        @if (!$isAvailable)
                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                        @endif
                    </a>
                </div>


                <div class="entry-title">
                    <h3>
                        <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                            {{ $content->variableLang($lang)->title }}
                        </a>
                    </h3>
                </div>

                <div class="entry-content">

                    @if(Request::segment(1)=="tr")
                    <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Devamı için tıklayınız...</a>
                    @elseif(Request::segment(1)=="en")
                    <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Click for more...</a>
                    @else
                    <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Devamı için tıklayınız...</a>
                    @endif
                </div>


            </div>
        </div>
        @endforeach


    </div>
</div>



</div>