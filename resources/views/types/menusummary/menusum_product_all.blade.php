@php
    //dump(json_decode($ths->props)->props_sum_type);
    //dump(json_decode($ths->props)->props_sum_count);

    $pscount = json_decode($ths->props)->props_sum_count;
@endphp



        @foreach ($menuSum->topHasSub as $ths)
            
            @php
                $content = $ths->subContent;
                if (empty($content->variableLang($lang))) {
                    $contVariable = $content->variable;
                }else{
                    $contVariable = $content->variableLang($lang);
                }
            @endphp

            @if($pscount == 2)
                <div class="col-lg-6">
            @elseif($pscount == 3)
                <div class="col-lg-4">
            @elseif($pscount == 4)
                <div class="col-lg-3">
            @endif
            
            

                <div class="entry clearfix">

                    <div class="entry-list-continer">

                        <div class="entry-image">
                            <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                @php $isAvailable = false; @endphp
                                @foreach ($content->subContentThs as $cths)
                                    @if($cths->subContent->type == 'photo')
                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content) }}" />
                                        @php $isAvailable = true; @endphp
                                        @break
                                    @elseif($cths->subContent->type == 'photogallery')
                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
                                        @php $isAvailable = true; @endphp
                                        @break
                                    @endif
                                @endforeach
                                @if (!$isAvailable)
                                    <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                                @endif
                            </a>
                        </div>
                        <div class="entry-title">
                            <h2>
                                <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                    {{ $content->variableLang($lang)->title }}
                                </a>
                            </h2>
                        </div>
                        <div class="entry-content">
                            @if($content->variableLang($lang)->short_content != "")
                                <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                    <p>{!! $content->variableLang($lang)->short_content !!}</p>
                                </a>
                            @endif
                            
                            <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Devamı için Tıklayınız...</a>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li>
                                <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                    @php
                                        if(!is_null($ths->top_menu_id)){
                                            $form_visible_data_count = App\FormData::where('source_type', 'content')->where('source_id', $ths->sub_content_id)->where('visible', 'yes')->count();
                                        }
                                        //dump($form_visible_data_count);
                                    @endphp
                                    @if($form_visible_data_count > 0)
                                        <i class="fas fa-comments"></i> {{ $form_visible_data_count }}
                                    @endif
                                </a>
                            </li>

                            @php 
                                $props = json_decode($content->variableLang($lang)->props);
                            @endphp

                            @php $curry = ''; @endphp
                            @if(!empty($props->props_currency))
                                
                                @if($props->props_currency == 'try')
                                    @php $curry = '₺'; @endphp
                                @elseif($props->props_currency == 'usd')
                                    @php $curry = '$'; @endphp
                                @elseif($props->props_currency == 'eur')
                                    @php $curry = '€'; @endphp
                                @endif

                            @endif

                            @if(!empty($props->props_price))

                                @if(empty($props->props_discounted_price))
                                    <li class="entry-meta-price">
                                        <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}"> 
                                             {{ number_format($props->props_price, 2, ',', '.') }} {{ $curry }}
                                        </a>
                                    </li>
                                @else
                                    <li class="entry-meta-price">
                                        <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}"> 
                                             <span>{{ number_format($props->props_price, 2, ',', '.') }} {{ $curry }}</span> {{ number_format($props->props_discounted_price, 2, ',', '.') }} {{ $curry }}
                                        </a>
                                    </li>
                                @endif
                            @endif
                        </ul>

                    </div>

                </div>


            </div>



        @endforeach

