@php
  // dump(json_decode($ths->props)->props_sum_type);
   //dd(json_decode($ths->props)->props_sum_count);
$col=json_decode($ths->props)->props_sum_count;

@endphp

<div class="col-lg-12">


    <div class="container clearfix">


        <div class="row">
         @foreach ($menuSum->topHasSub as $ths)

         @php
         $content = $ths->subContent;
         if (empty($content->variableLang($lang))) {
            $contVariable = $content->variable;
        }else{
            $contVariable = $content->variableLang($lang);
        }

        @endphp
        <div style="padding: 30px;" class="col-{{12/$col}}">
            <div class="feature-box fbox-plain">
                <div class="fbox-icon" data-animate="bounceIn">
                    <i class="{{json_decode($content->variableLang($lang)->props)->props_icon}} fa-2x"></i>
                </div>
                <h4>{{$content->variableLang($lang)->title}}</h4>

            </div>
        </div>



        @endforeach

    </div></div></div>
