@php
    //dump(json_decode($ths->props)->props_sum_type);
    //dump(json_decode($ths->props)->props_sum_count);
@endphp
<div class="{{ json_decode($ths->props)->props_sum_colvalue}}">

<div class="container clearfix">
    
    <div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="{{ json_decode($ths->props)->props_sum_count }}" data-items-lg="{{ json_decode($ths->props)->props_sum_count }}" data-items-xl="{{ json_decode($ths->props)->props_sum_count }}">

        @foreach ($menuSum->topHasSub as $ths)
            
            @php
                $content = $ths->subContent;
                if (empty($content->variableLang($lang))) {
                    $contVariable = $content->variable;
                }else{
                    $contVariable = $content->variableLang($lang);
                }
            @endphp

            <div class="oc-item">
                <div class="ipost clearfix">
                    <div class="entry-list-continer">
                        <div class="entry-image">
                            <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                @php $isAvailable = false; @endphp
                                @foreach ($content->subContentThs as $cths)
                                    @if($cths->subContent->type == 'photo')
                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content) }}" />
                                        @php $isAvailable = true; @endphp
                                        @break
                                    @elseif($cths->subContent->type == 'photogallery')
                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
                                        @php $isAvailable = true; @endphp
                                        @break
                                    @endif
                                @endforeach
                                @if (!$isAvailable)
                                    <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                                @endif
                            </a>
                        </div>
                        <div class="entry-title">
                            <h2>
                                <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                    {{ $content->variableLang($lang)->title }}
                                </a>
                            </h2>
                        </div>
                        <div class="entry-content">
                            @if($content->variableLang($lang)->short_content != "")
                                <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                    <p>{!! $content->variableLang($lang)->short_content !!}</p>
                                </a>
                            @endif
                            
                            <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Devamı için Tıklayınız...</a>
                        </div>
                        <ul class="entry-meta clearfix">
                            <li><i class="fas fa-calendar"></i> {{ $content->variableLang($lang)->created_at->format('d.m.Y') }} </li>
                            @php
                                if(!is_null($ths->top_menu_id)){
                                    $form_visible_data_count = App\FormData::where('source_type', 'content')->where('source_id', $ths->sub_content_id)->where('visible', 'yes')->count();
                                }
                            @endphp
                            @if($form_visible_data_count > 0)
                                <li>
                                    <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}#comments">
                                        <i class="fas fa-comments"></i> {{ $form_visible_data_count }} Yorum
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>    
                </div>
            </div>

        @endforeach

    </div>

</div>
</div>