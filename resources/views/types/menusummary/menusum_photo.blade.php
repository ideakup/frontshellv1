@php
  // dump(json_decode($ths->props)->props_sum_type);
   //dd(json_decode($ths->props)->props_sum_count);

@endphp

<div class="{{ json_decode($ths->props)->props_sum_colvalue}}">


<div class="container clearfix">

    <div id="oc-posts" class="owl-carousel posts-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="1" data-items-sm="2" data-items-md="{{ json_decode($ths->props)->props_sum_count }}" data-items-lg="{{ json_decode($ths->props)->props_sum_count }}" data-items-xl="{{ json_decode($ths->props)->props_sum_count +1}}">

     @foreach ($menuSum->topHasSub as $ths)

     @php
     $content = $ths->subContent;
     if (empty($content->variableLang($lang))) {
        $contVariable = $content->variable;
    }else{
        $contVariable = $content->variableLang($lang);
    }


    @endphp

    <div class="oc-item">
    <a href="{{json_decode($contVariable->props)->photo_url}}"target="_blank">
      <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$contVariable->content) }}" /> 
    </a>


    </div>
    @endforeach

</div></div>
</div>