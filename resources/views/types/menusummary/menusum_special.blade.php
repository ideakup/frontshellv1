<!-- aaa -->
<div class="{{ json_decode($ths->props)->props_sum_colvalue}}">


<div class="container clearfix">
    <div class="owl-carousel image-carousel carousel-widget flip-card-wrapper clearfix" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="2" data-items-sm="2" data-items-md="2" data-items-lg="{{ json_decode($ths->props)->props_sum_count }}" data-items-xl="{{ json_decode($ths->props)->props_sum_count }}" style="overflow: visible;">
        
        @foreach ($menuSum->topHasSub as $ths)
                
            @php
                $content = $ths->subContent;
                if (empty($content->variableLang($lang))) {
                    $contVariable = $content->variable;
                }else{
                    $contVariable = $content->variableLang($lang);
                }
            
                $isAvailable = false;
                foreach ($content->subContentThs as $cths){
                    if($cths->subContent->type == 'photo'){
                        $imgpath = env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content;
                        $isAvailable = true;
                        break;
                    }else if($cths->subContent->type == 'photogallery'){
                        $imgpath = env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url;
                        $isAvailable = true;
                        break;
                    }
                }
                if (!$isAvailable){
                    $imgpath = env('APP_UPLOAD_PATH_V3').'default.jpg';
                }
            @endphp
            

            <div class="flip-card">
                <div class="flip-card-front dark" style="background-image: url('{{ $imgpath }}')">
                    <div class="flip-card-inner">
                        <div class="card nobg noborder">
                            <div class="card-body">
                                <h3 class="card-title mb-0">{{ $content->variableLang($lang)->title }}</h3>
                                <span class="font-italic">
                                    @if($content->variableLang($lang)->short_content != "")
                                        {!! $content->variableLang($lang)->short_content !!}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flip-card-back bg-danger no-after">
                    <div class="flip-card-inner">
                        <p class="mb-2 text-white">
                            {{ $content->variableLang($lang)->title }}
                            @if($content->variableLang($lang)->short_content != "")
                               - {!! $content->variableLang($lang)->short_content !!}
                            @endif
                        </p>
                        <a href="{{ url($langSlug.'/'.$menuSum->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="btn btn-outline-light mt-2">Sayfaya Git</a>
                    </div>
                </div>
            </div>

        @endforeach

    </div>

</div>

</div>