@if(!is_null($menu->topHasSub->first()))
    @if($menu->topHasSub->first()->type == 'content' && $menu->topHasSub->first()->content->type == 'text' && json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_section == 'header-stick')
        <div class="section {{ json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->first()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif

@if($menu->variable->title != "")
    <div class="section notopmargin nobottommargin nobottompadding">
        <div class="{{ $wsConfig['containerClass'] }} clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $menu->variable->title }}</h2>
                </div>
            </div>
        </div>
    </div>
@endif

@php $_props_colortheme = ''; @endphp
@php $_isFirstSection = true; @endphp

@if ($menu->asidevisible == 'yes')
    <div class="container clearfix">
        <div class="postcontent nobottommargin clearfix">
@endif

            <div class="section notopmargin nobottommargin">
                <div class="{{ $wsConfig['containerClass'] }} clearfix">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="portfolio" class="portfolio grid-container @if ($menu->listtype == 'normal') portfolio-1 @elseif ($menu->listtype == 'col-2x') portfolio-2 @elseif ($menu->listtype == 'col-3x') portfolio-3 @elseif ($menu->listtype == 'col-4x') portfolio-4 @endif portfolio-nomargin clearfix">
                                
                                @foreach ($menu->topHasSub as $ths)
                                    
                                    @if (!is_null($ths->sub_content_id))
                                        
                                        @php
                                            $content = $ths->subContent;
                                        @endphp

                                        @if(!is_null($content) && $content->type == 'photogallery')
                                            
                                            @php
                                                if (empty($content->variableLang($lang))) {
                                                    $contVariable = $content->variable;
                                                }else{
                                                    $contVariable = $content->variableLang($lang);
                                                }
                                            @endphp

                                            <article class="portfolio-item">
                                                <div class="portfolio-image">
                                                    @if(!is_null($content->photogallery->first()))
                                                        <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                            <img src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $content->photogallery->first()->url }}" alt="" />
                                                            <div class="portfolio-overlay"> </div>
                                                        </a>
                                                    @endif
                                                </div>
                                                <div class="portfolio-desc">
                                                    <h3>
                                                        <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                            {{ $content->variableLang($lang)->title }}
                                                        </a>
                                                    </h3>
                                                    <span style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical; text-align: center;">
                                                        <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                            {{ $content->variableLang($lang)->short_content }}
                                                        </a>
                                                    </span>
                                                </div>
                                            </article>

                                        @endif

                                    @endif

                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @foreach ($menu->topHasSub as $ths)
                
                @if (!is_null($ths->sub_menu_id))
                    
                    {{ "mhc->type == 'menu'" }}
                    @php
                        /*
                        $menu = $ths->menu;
                    @endphp
                    
                    @if ($menu->type == 'photogallery')
                        @include('types.menu.photogallery')
                    @elseif (starts_with($menu->type, 'list'))
                        @include('types.menu.'.$menu->type)
                    @endif

                    @php
                        */
                    @endphp

                @elseif (!is_null($ths->sub_content_id))
                    
                    @php
                        $content = $ths->subContent;
                    @endphp

                    @if(!is_null($content) && $content->type != 'photogallery')

                        @php
                            if (empty($content->variableLang($lang))) {
                                $contVariable = $content->variable;
                            }else{
                                $contVariable = $content->variableLang($lang);
                            }
                        @endphp

                        @if(json_decode($content->variableLang($lang)->props)->props_section == 'container')

                            @if($_props_colortheme != json_decode($content->variableLang($lang)->props)->props_colortheme)
                                @if(!$_isFirstSection)
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @php
                                    $_isFirstSection = false;
                                    $_props_colortheme = json_decode($content->variableLang($lang)->props)->props_colortheme;
                                @endphp
                                    <div class="section {{ $_props_colortheme }} notopmargin nobottommargin">
                                        <div class="{{ $wsConfig['containerClass'] }} clearfix">
                                            <div class="row">
                            @endif

                            @if ($content->type == 'text')
                                @include('types.menupartials.text')
                            @elseif ($content->type == 'photo')
                                @include('types.menupartials.photo')
                            @elseif ($content->type == 'link')
                                @include('types.menupartials.link')
                            @elseif ($content->type == 'slide')
                                @php $slideData = $content->slide; @endphp
                                @include('types.menupartials.slide')
                            @elseif ($content->type == 'seperator')
                                @include('types.menupartials.seperator')
                            @elseif ($content->type == 'form')
                                @include('types.menupartials.form')
                            @elseif ($content->type == 'mapturkey')
                                @include('types.menupartials.mapturkey')
                            @elseif ($content->type == 'rssfeed')
                                @include('types.menupartials.rssfeed')
                            @endif

                            @if($loop->last)
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endif

                    @endif

                @endif
            @endforeach

@if ($menu->asidevisible == 'yes')
        </div>
        @include('partials.asidebar')
    </div>
@endif

@if(!is_null($menu->topHasSub->last()))
    @if($menu->topHasSub->last()->type == 'content' && $menu->topHasSub->last()->content->type == 'text' && json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @elseif($menu->topHasSub->last()->type == 'content' && $menu->topHasSub->last()->content->type == 'code' && json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section }} notopmargin nobottommargin notoppadding nobottompadding {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container_full clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
