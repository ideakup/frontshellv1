@php $_props_colortheme = ''; @endphp
@php $_isFirstSection = true; @endphp

@if($menu->variable->title != "")
    <div class="container clearfix">
        <div class="row">
            <div class="col-lg-12">
                <h2 style="margin: 15px;">{{ $menu->variable->title }}</h2>
            </div>
        </div>
    </div>
@endif

@if ($menu->asidevisible == 'yes')
    <div class="container list-default clearfix">
        <div class="postcontent nobottommargin clearfix">
@endif
            
            <div class="section notopmargin nobottommargin">
                <div class="{{ $wsConfig['containerClass'] }} clearfix">
                    @if($menu->listtype == 'normal')
                        <div id="posts">
                    @elseif($menu->listtype == 'col-2x')
                        <div id="posts" class="post-grid grid-container grid-2 clearfix" data-layout="fitRows">
                    @elseif($menu->listtype == 'col-3x')
                        <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
                    @elseif($menu->listtype == 'col-4x')
                        <div id="posts" class="post-grid grid-container grid-4 clearfix" data-layout="fitRows">
                    @endif

                        
                            
                        @php
                            $tagVariable = App\TagVariable::where('slug', $attr)->first();
                        @endphp

                        @foreach ($tagVariable->tag->contentHasTag as $cht)

                            @php
                                $content = $cht->content;
                                if (empty($content->variableLang($lang))) {
                                    $contVariable = $content->variable;
                                }else{
                                    $contVariable = $content->variableLang($lang);
                                }

                                $tempThs = $content->topContentThs->first();
                                $tempMenu = $content->topContentThs->first()->topMenu;
                                if (empty($tempMenu->variableLang($lang))) {
                                    $tempMenuVariable = $tempMenu->variable;
                                }else{
                                    $tempMenuVariable = $tempMenu->variableLang($lang);
                                }
                            @endphp

                            <div class="entry clearfix">
                                <div class="entry-list-continer">

                                    <div class="entry-image">
                                        @if($content->type == 'group')
                                            <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                @php $isAvailable = false; @endphp
                                                @foreach ($content->subContentThs as $cths)
                                                    @if($cths->subContent->type == 'photo')
                                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content) }}" />
                                                        @php $isAvailable = true; @endphp
                                                        @break
                                                    @elseif($cths->subContent->type == 'photogallery')
                                                        <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
                                                        @php $isAvailable = true; @endphp
                                                        @break
                                                    @endif
                                                @endforeach
                                                @if (!$isAvailable)
                                                    <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                                                @endif
                                            </a>
                                        @elseif($content->type == 'photogallery')
                                            <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                <img src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $content->photogallery->first()->url }}" alt="" />
                                            </a>
                                        @endif
                                    </div>
                                    <div class="entry-title">
                                        <h2>
                                            <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                {{ $content->variableLang($lang)->title }}
                                            </a>
                                        </h2>
                                    </div>
                                    <div class="entry-content">
                                        @if($content->variableLang($lang)->short_content != "")
                                            <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                <p style="overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;">{!! $content->variableLang($lang)->short_content !!}</p>
                                            </a>
                                        @endif
                                        
                                        <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}"class="more-link">Devamı için Tıklayınız...</a>
                                    </div>
                                    <ul class="entry-meta clearfix">
                                        <li><i class="fas fa-calendar"></i> {{ $content->variableLang($lang)->created_at->format('d.m.Y') }} </li>

                                        @php
                                            if(!is_null($tempThs->top_menu_id)){
                                                $form_visible_data_count = App\FormData::where('source_type', 'content')->where('source_id', $tempThs->sub_content_id)->where('visible', 'yes')->count();
                                            }
                                        @endphp
                                        @if($form_visible_data_count > 0)
                                            <li>
                                                <a href="{{ url($tempMenuVariable->slug.'/'.$content->variableLang($lang)->slug) }}#comments">
                                                    <i class="fas fa-comments"></i> {{ $form_visible_data_count }} Yorum
                                                </a>
                                            </li>
                                        @endif

                                        <style type="text/css">
                                            .li-right{
                                                float: right !important;
                                            }

                                            .li-right:before{
                                                content: '' !important;
                                            }
                                        </style>


                                        <li class="li-right">
                                            <a href="{{ url($tempMenuVariable->slug) }}">
                                                {{ $tempMenuVariable->menutitle }}
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

@if ($menu->asidevisible == 'yes')
        </div>
        @include('partials.asidebar')
    </div>
@endif