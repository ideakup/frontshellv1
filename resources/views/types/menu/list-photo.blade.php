
@php
//dd($menu->topHasSub->subContent);
@endphp

<div class="container clearfix">

  <ul class="clients-grid nobottommargin clearfix">

   @foreach ($menu->topHasSub as $ths)

   @php
   $content = $ths->subContent;
   if (empty($content->variableLang($lang))) {
    $contVariable = $content->variable;
  }else{
    $contVariable = $content->variableLang($lang);
  }

  //dd(json_decode($contVariable));
  @endphp
  <li>
    <a href="{{json_decode($contVariable->props)->photo_url}}"target="_blank">
      <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$contVariable->content) }}" /> 
    </a>
  </li>


  @endforeach

</ul>

</div>

<!--
<ul class="clients-grid nobottommargin clearfix">
  <li><a href="#"><img src="images/clients/1.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/2.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/3.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/4.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/5.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/6.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/7.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/8.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/9.png" alt="Clients"></a></li>
  <li><a href="#"><img src="images/clients/9.png" alt="Clients"></a></li>

  <li class="d-none d-md-block"><a href="#"><img src="images/clients/10.png" alt="Clients"></a></li>
</ul>
-->