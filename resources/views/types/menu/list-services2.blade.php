

<div class="container clearfix">

    <div class="row">  

    @foreach ($menu->topHasSub as $ths)

        @php
        $content = $ths->subContent;
        if (empty($content->variableLang($lang))) {
            $contVariable = $content->variable;
        }else{
            $contVariable = $content->variableLang($lang);
        }
        
        $isAvailable = false;
        foreach ($content->subContentThs as $cths){
            if($cths->subContent->type == 'photo'){
                $imgpath = env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content;
                $isAvailable = true;
                break;
            }else if($cths->subContent->type == 'photogallery'){
                $imgpath = env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url;
                $isAvailable = true;
                break;
            }
        }
        if (!$isAvailable){
            $imgpath = env('APP_UPLOAD_PATH_V3').'default.jpg';
        }
        @endphp
        
        <div  class="{{json_decode($content->variableLang($lang)->props)->props_colvalue}}">

            <div class="price-list shadow-sm card border-0 rounded ">
            <div  class="position-relative ">
                <img src="{{ $imgpath }}" alt="Featured image 1" class="card-img-top rounded-top">
                <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                    <h3 class="card-title mb-0 text-white">{{ $content->variableLang($lang)->title }}</h3>

                </div>
            </div>
            <div class="card-body">

             <div class="entry-content">
                @if($content->variableLang($lang)->short_content != "")
                <p>{!! $content->variableLang($lang)->short_content !!}</p>
                @endif
            </div>

        </div>
    </div>        </div>

        @endforeach
    </div></div></div>



