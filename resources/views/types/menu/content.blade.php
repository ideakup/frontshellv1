@php
  //dd($menu->topHasSub->first()->content->type);
@endphp

@if(!is_null($menu->topHasSub->first()))
    @if($menu->type == 'content' && $menu->topHasSub->first()->subContent->type == 'text' && json_decode($menu->topHasSub->first()->subContent->variableLang($lang)->props)->props_section == 'header-stick')
        <div class="section {{ json_decode($menu->topHasSub->first()->subContent->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->first()->subContent->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->first()->subContent->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif

@if($menu->variable->title != "")
    <div class="section notopmargin nobottommargin nobottompadding">
        <div class="{{ $wsConfig['containerClass'] }} clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $menu->variable->title }}</h2>
                </div>
            </div>
        </div>
    </div>
@endif

@php $_props_colortheme = ''; @endphp
@php $_isFirstSection = true; @endphp

@if ($menu->asidevisible == 'yes')
    <div class="container clearfix">
        <div class="postcontent nobottommargin clearfix">
@endif

@foreach ($menu->topHasSub as $ths)

    @if (!is_null($ths->sub_menu_id))
        
        @if(empty(json_decode($ths->props)->props_sum_type))
            @php
                $menu = $ths->subMenu;
            @endphp
            @if ($menu->type == 'photogallery')
                @include('types.menu.photogallery')
            @elseif (starts_with($menu->type, 'list'))
                @include('types.menu.'.$menu->type)
            @endif
        @else
            <!-- Özet Menü Item -->
            @php
                $menuSum = $ths->subMenu;
            @endphp
           
            @if (starts_with($menuSum->type, 'list')||$menuSum->type == 'photogallery')
                @include('types.menusummary.'.json_decode($ths->props)->props_sum_type)
            @endif

        @endif

    @elseif (!is_null($ths->sub_content_id))
        
        @php
            $content = $ths->subContent;
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
        @endphp

        @if(json_decode($content->variableLang($lang)->props)->props_section == 'container')

            @if($_props_colortheme != json_decode($content->variableLang($lang)->props)->props_colortheme)
                @if(!$_isFirstSection)
                            </div>
                        </div>
                    </div>
                @endif
                @php
                    $_isFirstSection = false;
                    $_props_colortheme = json_decode($content->variableLang($lang)->props)->props_colortheme;
                @endphp
                    <div class="section {{ $_props_colortheme }} {{ $slug }} notopmargin nobottommargin">
                        <div class="{{ $wsConfig['containerClass'] }} clearfix">
                            <div class="row">
            @endif
            
            @if ($content->type == 'text')
                @include('types.menupartials.text')
            @elseif ($content->type == 'photo')
                @include('types.menupartials.photo')
            @elseif ($content->type == 'photogallery')
                @include('types.menupartials.photogallery')
            @elseif ($content->type == 'link')
                @include('types.menupartials.link')
            @elseif ($content->type == 'slide')
                @php $slideData = $content->slide; @endphp
                @include('types.menupartials.slide')
            @elseif ($content->type == 'seperator')
                @include('types.menupartials.seperator')
            @elseif ($content->type == 'form')
                @include('types.menupartials.form')
            @elseif ($content->type == 'mapturkey')
                @include('types.menupartials.mapturkey')
            @elseif ($content->type == 'rssfeed')
                @include('types.menupartials.rssfeed')
            @elseif ($content->type == 'code')
                @include('types.menupartials.code')
            @endif
            
        @endif

    @endif

    @if($loop->last)
  
                </div>
            </div>
        </div>
    @endif

@endforeach

@if ($menu->asidevisible == 'yes')
        </div>
        @include('partials.asidebar')
    </div>
@endif

@if(!is_null($menu->topHasSub->last()))
    @if($menu->type == 'content' && $menu->topHasSub->last()->subContent->type == 'text' && json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->subContent->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @elseif($menu->type == 'content' && $menu->topHasSub->last()->subContent->type == 'code' && json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_section }} notopmargin nobottommargin notoppadding nobottompadding {{ json_decode($menu->topHasSub->last()->subContent->variableLang($lang)->props)->props_colortheme }}">
            <div class="container_full clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->subContent->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif