
@php
//dd($menu->topHasSub->subContent);
@endphp

<div class="container clearfix">

	<div class="row">   
		@foreach ($menu->topHasSub as $ths)

		@php
		$content = $ths->subContent;
		if (empty($content->variableLang($lang))) {
			$contVariable = $content->variable;
		}else{
			$contVariable = $content->variableLang($lang);
		}
		@endphp
		<div style="padding: 0px 30px 0px 30px;" class="{{json_decode($content->variableLang($lang)->props)->props_colvalue}}">
			
			<div class="feature-box fbox-large fbox-plain fbox-dark">
				<div class="fbox-icon">
					<i class="{{json_decode($content->variableLang($lang)->props)->props_icon}} fa-5x"></i>
				</div>
				<h3>{{$contVariable->title}}</h3>
				
				<p>{{$contVariable->short_content}}</p>

				
			</div>
		</div>



		@endforeach

	</div>

</div>

