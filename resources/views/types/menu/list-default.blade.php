


@if(!is_null($menu->topHasSub->first()))
    @if($menu->topHasSub->first()->type == 'content' && $menu->topHasSub->first()->content->type == 'text' && json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_section == 'header-stick')
        <div class="section {{ json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->first()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->first()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif

@if($menu->variable->title != "")
    <div class="section notopmargin nobottommargin nobottompadding">
        <div class="{{ $wsConfig['containerClass'] }} clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <h2>{{ $menu->variable->title }}</h2>
                </div>
            </div>
        </div>
    </div>
@endif

@php $_props_colortheme = ''; @endphp
@php $_isFirstSection = true; @endphp

@if ($menu->asidevisible == 'yes')
    <div class="container list-default clearfix">
        <div class="postcontent nobottommargin clearfix">
@endif

            <div class="section notopmargin nobottommargin">
                <div class="{{ $wsConfig['containerClass'] }} clearfix">
                    @if($menu->listtype == 'normal')
                        <div id="posts">
                    @elseif($menu->listtype == 'col-2x')
                        <div id="posts" class="post-grid grid-container grid-2 clearfix" data-layout="fitRows">
                    @elseif($menu->listtype == 'col-3x')
                        <div id="posts" class="post-grid grid-container grid-3 clearfix" data-layout="fitRows">
                    @elseif($menu->listtype == 'col-4x')
                        <div id="posts" class="post-grid grid-container grid-4 clearfix" data-layout="fitRows">
                    @endif

                        @foreach ($menu->topHasSub as $ths)
                            
                            @php
                                $content = $ths->subContent;
                                if (empty($content->variableLang($lang))) {
                                    $contVariable = $content->variable;
                                }else{
                                    $contVariable = $content->variableLang($lang);
                                }
                            @endphp

                            <div class="entry clearfix">
                                <div class="entry-list-continer">

                                    <div class="entry-image">
                                        <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                            @php $isAvailable = false; @endphp
                                            @foreach ($content->subContentThs as $cths)
                                                @if($cths->subContent->type == 'photo')
                                                    <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->variableLang($lang)->content) }}" />
                                                    @php $isAvailable = true; @endphp
                                                    @break
                                                @elseif($cths->subContent->type == 'photogallery')
                                                    <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
                                                    @php $isAvailable = true; @endphp
                                                    @break
                                                @endif
                                            @endforeach
                                            @if (!$isAvailable)
                                                <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
                                            @endif
                                        </a>
                                    </div>
                                    <div class="entry-title">
                                        <h2>
                                            <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                {{ $content->variableLang($lang)->title }}
                                            </a>
                                        </h2>
                                    </div>
                                    <div class="entry-content">
                                        @if($content->variableLang($lang)->short_content != "")
                                            <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}">
                                                <p>{!! $content->variableLang($lang)->short_content !!}</p>
                                            </a>
                                        @endif
                                        
                                        <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}" class="more-link">Devamı için Tıklayınız... </a>   <span class="float-right"> <i class="fas fa-calendar"></i> {{ $content->variableLang($lang)->created_at->format('d.m.Y') }} </span> 
                                    </div> 
                                    <ul class="entry-meta clearfix">
                                        @php
                                            if(!is_null($ths->top_menu_id)){
                                                $form_visible_data_count = App\FormData::where('source_type', 'content')->where('source_id', $ths->sub_content_id)->where('visible', 'yes')->count();
                                            }
                                        @endphp
                                        @if($form_visible_data_count > 0)
                                            <li>
                                                <a href="{{ url($langSlug.'/'.$menu->variableLang($lang)->slug.'/'.$content->variableLang($lang)->slug) }}#comments">
                                                    <i class="fas fa-comments"></i> {{ $form_visible_data_count }} Yorum
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                    
                                </div>
                            </div>

                        @endforeach

                    </div>
                </div>
            </div>

@if ($menu->asidevisible == 'yes')
        </div>
        @include('partials.asidebar')
    </div>
@endif

@if(!is_null($menu->topHasSub->last()))
    @if($menu->topHasSub->last()->type == 'content' && $menu->topHasSub->last()->content->type == 'text' && json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section }} nobottommargin {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @elseif($menu->topHasSub->last()->type == 'content' && $menu->topHasSub->last()->content->type == 'code' && json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section == 'footer-stick')
        <div class="section {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_section }} notopmargin nobottommargin notoppadding nobottompadding {{ json_decode($menu->topHasSub->last()->content->variableLang($lang)->props)->props_colortheme }}">
            <div class="container_full clearfix">
                <div class="row">
                    <div class="col-lg-12">
                        {!! $menu->topHasSub->last()->content->variableLang($lang)->content !!}
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif