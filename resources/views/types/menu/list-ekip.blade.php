
@php
//dd($content->variableLang($lang)->props);
@endphp

<div class="container clearfix">
  <h2>KURUCU AVUKAT</h2>   

  <div class="row">

    @foreach ($menu->topHasSub as $ths)

    @php
    $content = $ths->subContent;
    if (empty($content->variableLang($lang))) {
      $contVariable = $content->variable;
    }else{
      $contVariable = $content->variableLang($lang);
    }
    $stafftype=json_decode($content->variableLang($lang)->props)->props_stafftype;
    @endphp
    @if($stafftype=="kurucu_avukat")
    <div class="space-div col-lg-1 "></div>
    <div class="teams col-lg-10 bottommargin">

      <div class="team team-list clearfix">
        <div class="team-image">
          @php $isAvailable = false; @endphp
          @foreach ($content->subContentThs as $cths)
          @if($cths->subContent->type == 'photo')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'small/'.$cths->subContent->variableLang($lang)->content) }}" />
          @php $isAvailable = true; @endphp
          @break
          @elseif($cths->subContent->type == 'photogallery')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
          @php $isAvailable = true; @endphp
          @break
          @endif
          @endforeach
          @if (!$isAvailable)
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
          @endif
        </div>
        <div class="team-desc">
          <div class="team-title"><h4>{{$contVariable->title}}</h4><span>{{json_decode($content->variableLang($lang)->props)->props_title}}  </span></div>
          <div class="team-content">
            @foreach ($content->subContentThs as $cths)

            @if($cths->subContent->type=="text")
            {!!$cths->subContent->variableLang($lang)->content!!}
            @endif


            @endforeach

          </div>

        </div>
      </div>

    </div>  


    <div class=" space-div col-lg-1"></div>
    @endif
    @endforeach

  </div>



  <h2>AVUKATLAR</h2>   

  <div class="row">
    @foreach ($menu->topHasSub as $ths)

    @php
    $content = $ths->subContent;
    if (empty($content->variableLang($lang))) {
      $contVariable = $content->variable;
    }else{
      $contVariable = $content->variableLang($lang);
    }
    $stafftype=json_decode($content->variableLang($lang)->props)->props_stafftype;
    @endphp
    @if($stafftype=="avukat")
    <div class="space-div col-lg-1"></div>
    <div class="teams col-lg-10 bottommargin">

      <div class="team team-list clearfix">
        <div class="team-image">
          @php $isAvailable = false; @endphp
          @foreach ($content->subContentThs as $cths)
          @if($cths->subContent->type == 'photo')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'small/'.$cths->subContent->variableLang($lang)->content) }}" />
          @php $isAvailable = true; @endphp
          @break
          @elseif($cths->subContent->type == 'photogallery')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
          @php $isAvailable = true; @endphp
          @break
          @endif
          @endforeach
          @if (!$isAvailable)
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
          @endif
        </div>
        <div class="team-desc">
          <div class="team-title"><h4>{{$contVariable->title}}</h4><span>{{json_decode($content->variableLang($lang)->props)->props_title}}  </span></div>
          <div class="team-content">
            @foreach ($content->subContentThs as $cths)

            @if($cths->subContent->type=="text")
            {!!$cths->subContent->variableLang($lang)->content!!}
            @endif


            @endforeach

          </div>

        </div>
      </div>

    </div>  


    <div class=" space-div col-lg-1"></div>
    @endif
    @endforeach

  </div>

  <h2>PERSONEL</h2>   

  <div class="row">
    @foreach ($menu->topHasSub as $ths)

    @php
    $content = $ths->subContent;
    if (empty($content->variableLang($lang))) {
      $contVariable = $content->variable;
    }else{
      $contVariable = $content->variableLang($lang);
    }
    $stafftype=json_decode($content->variableLang($lang)->props)->props_stafftype;
    @endphp
    @if($stafftype=="personel")
    <div class="space-div col-lg-1"></div>
    <div class="teams col-lg-10 bottommargin">

      <div class="team team-list clearfix">
        <div class="team-image">
          @php $isAvailable = false; @endphp
          @foreach ($content->subContentThs as $cths)
          @if($cths->subContent->type == 'photo')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'small/'.$cths->subContent->variableLang($lang)->content) }}" />
          @php $isAvailable = true; @endphp
          @break
          @elseif($cths->subContent->type == 'photogallery')
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$cths->subContent->photogallery->first()->url) }}" />
          @php $isAvailable = true; @endphp
          @break
          @endif
          @endforeach
          @if (!$isAvailable)
          <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.jpg') }}" />
          @endif
        </div>
        <div class="team-desc">
          <div class="team-title"><h4>{{$contVariable->title}}</h4><span>{{json_decode($content->variableLang($lang)->props)->props_title}}  </span></div>
          <div class="team-content">
            @foreach ($content->subContentThs as $cths)

            @if($cths->subContent->type=="text")
            {!!$cths->subContent->variableLang($lang)->content!!}
            @endif


            @endforeach

          </div>

        </div>
      </div>

    </div>  


    <div class=" space-div col-lg-1"></div>
    @endif
    @endforeach

  </div>

</div>
