<div class="single-post nobottommargin">
	<div class="entry clearfix">


		<style type="text/css">
			.ppp{
				position: absolute;
				z-index: 190;
				top: 0;
				left: 120px;
				background-color: #FFFFFF;
				width: 300px;
				height: 500px;
				padding: 0 20px;
				border-bottom: #efeae4;
			}
			.ppp .entry-title{
				margin-top: 20px;
			}
			.ppp .entry-image{
				margin-bottom: 0;
			}
		</style>
		@php
			$name = $contVariable->title;
			$short_content = $contVariable->short_content;
		@endphp



		<!--
			@if ($contVariable->title != '')
				<div class="entry-title">
					<h2>{{ $contVariable->title }}</h2>
				</div>
			@endif
		-->

		@php $iltrFormat = 'no'; @endphp
		@if($content->sub_content->count() > 1)
			@if ($content->sub_content[0]->type == 'slide' && $content->sub_content[1]->type == 'photo')
				@php $iltrFormat = 'yes'; @endphp
			@endif
		@endif

		@foreach ($content->sub_content as $element)

			@php
                if (empty($element->variableLang($lang))) {
                    $contVariable = $element->variable;
                }else{
                    $contVariable = $element->variableLang($lang);
                }
                //echo ' -('.$contVariable->title.')- ';
            @endphp
		
			@if ($loop->index == 0 && $element->type == 'slide' && $iltrFormat == 'yes')
				
				@php $slideData = $element->slide; @endphp
                @include('types.menupartials.slide')

			@elseif ($loop->index == 1 && $element->type == 'photo' && $iltrFormat == 'yes')
				
				<div class="ppp">
					@include('types.menupartials.photo')
					<div class="entry-title">
                        <h2 style="margin-bottom: 10px;"> {{ $name }} </h2>
                        <h4 style="text-align: center;"> {{ $short_content }} </h4>
                    </div>
				</div>

			@elseif ($element->type == 'text')
                @include('types.menupartials.text')
            @elseif ($element->type == 'photo')
                @include('types.menupartials.photo')
            @elseif ($element->type == 'photogallery')
                @include('types.menupartials.photogallery')
            @elseif ($element->type == 'link')
                @include('types.menupartials.link')
            @elseif ($element->type == 'slide')
               	@php $slideData = $element->slide; @endphp
                @include('types.menupartials.slide')
            @elseif ($element->type == 'seperator')
                @include('types.menupartials.seperator')
            @elseif ($element->type == 'form')
                @include('types.menupartials.form')
            @endif

		@endforeach


		<div class="entry-content notopmargin" style="display:none;">

			@foreach ($content->sub_content as $element)

				@if ($element->type == 'photo')
					@if ($loop->first && $element->type == 'photo' && $iltrFormat == 'yes')
						<div class="entry-image alignleft">
							<img src="{{ url(env('APP_UPLOAD_PATH_V3').'xlarge/'.$element->variableLang($lang)->content) }}" alt="" />
						</div>
					@endif
				@elseif($element->type == 'text')
					
					{!! $element->variableLang($lang)->content !!}

				@endif

			@endforeach

		</div>
	





	</div><!-- .entry end -->

</div>


