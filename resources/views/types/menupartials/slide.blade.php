<!-- OK -->
<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">
	<section id="slider" class="slider-element swiper_wrapper clearfix" data-loop="true" data-autoplay="7000" data-speed="650">
        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">

                @foreach ($slideData as $sElement)
                    @if (!is_null($sElement->image_url))

                        <div class="swiper-slide @if($sElement->theme == 'light') dark @endif" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $sElement->image_url }}');">
                            <div class="container clearfix">
                                <div class="slider-caption @if($sElement->align == 'center') slider-caption-center @elseif($sElement->align == 'right') slider-caption-right @endif ">

                                    <h2 data-animate="fadeInUp">{!! $sElement->title !!}</h2>
                                    <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">{!! $sElement->description !!}</p>
                                    @if (!empty($sElement->button_text))

                                        <a href="{{ $sElement->button_url }}" data-animate="fadeInUp" data-delay="400" class="button button-border button-rounded button-fill fill-from-bottom @if($sElement->theme == 'light') button-custom-light @else button-custom-dark @endif">
                                            <span>{{ $sElement->button_text }}</span>
                                        </a>

                                    @endif
                                    
                                </div>
                            </div>
                        </div>

                    @endif
                @endforeach

            </div>
            <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
            <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
            <div class="swiper-pagination"></div>
        </div>
    </section>
</div>