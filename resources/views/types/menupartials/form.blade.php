<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">

    @if (is_null($attr))
        @php
            $source_type = 'menu';
            $source_id = $menu->id;
        @endphp
    @else
        @php
            $source_type = 'content';
            $source_id = $ths->top_content_id;
        @endphp
    @endif

    <script>
        var formData_{{ $contVariable->content_id }} = '{!! $contVariable->content !!}';
    </script>

    <div class="form-widget">
        <form class="nobottommargin" id="form_{{ $contVariable->content_id }}" method="POST" action="{{ url($lang.'/form_save') }}">
            {{ csrf_field() }}
            <input type="hidden" name="lang" value="{{ $lang }}">
            <input type="hidden" name="source_type" value="{{ $source_type }}">
            <input type="hidden" name="source_id" value="{{ $source_id }}">
            <input type="hidden" name="form_id" value="{{ $contVariable->content_id }}">

            <div class="form-process"></div>

            <div id="form_element_{{ $contVariable->content_id }}"></div>
            <div class="col_full">
                <button type="button" id="form_submit_{{ $contVariable->content_id }}" name="form_submit_{{ $contVariable->content_id }}" class="button button-rounded button-reveal button-border" value="submit"> <i class="icon-paperplane"></i><span> {{ json_decode($ths->props)->props_buttonname }} </span></button>
            </div>

        </form>
    </div>

    @php
        if(is_null($ths->top_content_id)){
            $form_visible_data_count = App\FormData::where('source_type', 'menu')->where('source_id', $ths->top_menu_id)->where('visible', 'yes')->count();
            $form_visible_data = App\FormData::where('source_type', 'menu')->where('source_id', $ths->top_menu_id)->where('visible', 'yes')->orderBy('updated_at', 'desc')->get();
        }else{
            $form_visible_data_count = App\FormData::where('source_type', 'content')->where('source_id', $ths->top_content_id)->where('visible', 'yes')->count();
            $form_visible_data = App\FormData::where('source_type', 'content')->where('source_id', $ths->top_content_id)->where('visible', 'yes')->orderBy('updated_at', 'desc')->get();
        }
    @endphp

    @if(isset(json_decode($ths->props)->props_comment_status))
        @if (json_decode($ths->props)->props_comment_status == 'active' && $form_visible_data_count > 0)
            <div class="line"></div>
            <div class="clear"></div>
            
            <ol class="commentlist clearfix">
                @foreach($form_visible_data as $fvd)  
                    @php
                        //dump(json_decode($fvd->data, true));
                        //dump(json_decode($ths->props)->props_visible_elemenets[0]);
                        //dump(json_decode($fvd->data, true)[json_decode($ths->props)->props_visible_elemenets[0]]);
                    @endphp      
                    <li class="comment">

                        <div class="comment-wrap clearfix">

                            <div class="comment-meta">

                                <div class="comment-author vcard">

                                    <span class="comment-avatar clearfix">
                                    <img alt="" src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" class="avatar avatar-60 photo avatar-default" height="60" width="60"></span>

                                </div>

                            </div>

                            <div class="comment-content clearfix">

                                <div class="comment-author">{{ json_decode($fvd->data, true)[json_decode($ths->props)->props_visible_elemenets[0]] }}<span><a href="#" title="Permalink to this comment">{{ $fvd->updated_at->format('d.m.Y') }}</a></span></div>

                                <p>Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

                            </div>

                        </div>

                    </li>
                @endforeach
            </ol>
        @endif
    @endif

</div>



