<!-- OK -->
<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">
	@php
	    $rss = FeedReader::read(json_decode($content->variableLang($lang)->content)->content_url);
	    $ss = simplexml_load_string($rss->raw_data);
	//    dd($rss);
	@endphp
	<h3>{{ $content->variableLang($lang)->title }}</h3>
	<ul class="iconlist iconlist-large iconlist-color">
	    @foreach ($ss->channel->item as $rss_item)
	        @if($loop->index == json_decode($content->variableLang($lang)->content)->content_rowcount)
	            @break
	        @endif
	        <li>
	        	<i class="icon-car"></i>
	        	<a href="{{ $rss_item->link }}" target="_blank">
	        		<div style="display: inline-block; white-space: nowrap; width: 75%; overflow: hidden; text-overflow: ellipsis; ">
	        			{{ $rss_item->title }}
	        		</div>
	        	</a> 
	        	<small style="float: right;">
	        		{{ str_limit($rss_item->pubDate, 10, '') }}
	        	</small>
	        </li>
	    @endforeach
	</ul>
</div>