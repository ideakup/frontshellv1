<!-- OK -->

@php

	$district_data = array();
	$_districts = App\District::where('status', 'active')->where('deleted', 'no')->orderBy('order', 'asc')->get();
	
	foreach ($_districts as $district) {
		$district_data_temp = null;
		$dist_var = $district->variable;
		$district_data_temp['name'] = $dist_var->name;
		$district_data_temp['clickUrl'] = url($menu->variable->slug.'/'.$content->variable->slug.'/bolge/'.$dist_var->slug);

		$staffs = App\Staff::select('city_id')->distinct()->where('district_id', $district->id)->where('status', 'active')->where('deleted', 'no')->orderBy('city_id')->get();

		foreach ($staffs as $staff) {
			if(strlen($staff->city_id) == 1){
			  	$id = '0'.$staff->city_id;
			}else{
			  	$id = ''.$staff->city_id;
			}
			$district_data_temp['data'][] = array(
				'title' => $staff->city->variable->name, 
				'id' => 'TR-'.$id,
				'groupId' => $district->id,
				'clickUrl' => url($menu->variable->slug.'/'.$content->variable->slug.'/bolge/'.$district->variable->slug),
			);
		}
		$district_data[] = $district_data_temp;
	}

	$staffsNoDist = App\Staff::select('city_id')->distinct()->whereNull('district_id')->where('status', 'active')->where('deleted', 'no')->orderBy('city_id')->get();
	$fake_id = 101;
	foreach ($staffsNoDist as $staff) {
		$district_data_temp = null;
		$staffsNoDistCont = App\Staff::where('city_id', $staff->city_id)->where('district_id', '!=', NULL)->where('status', 'active')->where('deleted', 'no')->orderBy('city_id')->count();

		if($staffsNoDistCont == 0){
			if(strlen($staff->city_id) == 1){
			  	$id = '0'.$staff->city_id;
			}else{
			  	$id = ''.$staff->city_id;
			}
			$district_data_temp['name'] = '';
			$district_data_temp['clickUrl'] = NULL;
			$district_data_temp['data'][] = array(
				'title' => $staff->city->variable->name, 
				'id' => 'TR-'.$id,
				'groupId' => $fake_id,
				'clickUrl' => url($menu->variable->slug.'/'.$content->variable->slug.'/il/'.$staff->city->variable->slug),
			);
			$fake_id++;
			$district_data[] = $district_data_temp;
		}
	}

	$marker_city_data = array();
	$marker_county_data = array();

	$select_city_data = array();
	//$marker_county_data = array();

	$staffsMarker = App\Staff::select('city_id', 'county_id')->distinct()->where('status', 'active')->where('deleted', 'no')->orderBy('city_id')->get();

	foreach ($staffsMarker as $marker) {

		$type = '';
		if (is_null($marker->county_id)) {
			$rec = App\City::find($marker->city_id);
			$marker_city_data[] = [
				"idd" => $rec->id,
				"sslug" => $rec->variable->slug,
				"stitle" => $rec->variable->name,
				"title" => $rec->variable->name." Acente",
				"latitude" => $rec->latitude,
				"longitude" => $rec->longitude,
				'clickUrl' => url($menu->variable->slug.'/'.$content->variable->slug.'/ildetay/'.$rec->variable->slug),
			];
		}else{
			$rec = App\County::find($marker->county_id);
			$marker_county_data[] = [
				"idd" => $rec->id,
				"city_idd" => $rec->city->id,
				"sslug" => $rec->variable->slug,
				"stitle" => $rec->city->variable->name.' - '.$rec->variable->name,
				"title" => $rec->variable->name." Acente",
				"latitude" => $rec->latitude,
				"longitude" => $rec->longitude,
				'clickUrl' => url($menu->variable->slug.'/'.$content->variable->slug.'/ilcedetay/'.$rec->variable->slug),
			];
		}
	}

	//dd($district_data);

@endphp

<div class="col-md-12">

	<form method="GET" action="{{ url($menu->variable->slug.'/'.$content->variable->slug) }}" id="mapturkeyForm">
		{{ csrf_field() }}
		<div class="form-row">
			<div class="form-group col-lg-5 offset-md-1">
				<label>Şehirler ve İlçeler</label>
				<select id="sehirselect" name="sehirselect" class="form-control">
					<option selected="">Seçiniz...</option>
					@foreach($marker_city_data as $mmm)
						
						<option value="{{ $mmm['clickUrl'] }}"> {{ $mmm["stitle"] }} </option>
						
						@foreach($marker_county_data as $ggg)
							@if($ggg["city_idd"] == $mmm["idd"])
								<option value="{{ $ggg['clickUrl'] }}"> {{ $ggg["stitle"] }} </option>
							@endif
						@endforeach

					@endforeach
				</select>
			</div>
			<div class="form-group col-lg-5">
				<label>Bölge Müdürlükleri</label>
				<select id="bolgeselect" name="bolgeselect" class="form-control">
					<option selected="">Seçiniz...</option>
					@foreach($district_data as $district)
						@if(!is_null($district['clickUrl']))
							<option value="{{ $district['clickUrl'] }}"> {{ $district['name'] }} </option>
						@endif
					@endforeach
				</select>
			</div>
		</div>

	</form>
</div>

<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">

	<div id="chartdiv" @if (!is_null($attr) && $content->type == 'mapturkey') class="chart_div_mini" @endif></div>
	
</div>

<script>
	var bolgeler = <?php echo json_encode($district_data, JSON_NUMERIC_CHECK); ?>;
	var marker_city_data = <?php echo json_encode($marker_city_data, JSON_NUMERIC_CHECK); ?>;
	var marker_county_data = <?php echo json_encode($marker_county_data, JSON_NUMERIC_CHECK); ?>;
</script>