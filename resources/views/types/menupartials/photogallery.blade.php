<!-- OK -->
<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">
	<h2>{{ $contVariable->title }}</h2>
	@php
		$_colValue = '3';
		$c_v=json_decode($content->variableLang($lang)->props)->props_type;//propstype c_v değişkenine atandı.
	@endphp
	@if ($c_v == 'col-2x')
		@php $_colValue = '2';@endphp
	@elseif($c_v == 'col-3x')
		@php $_colValue = '3';@endphp
	@elseif($c_v == 'col-4x')
		@php $_colValue = '4';@endphp
	@endif

	
	<div class="masonry-thumbs grid-{{ $_colValue }}" data-lightbox="gallery">
		@foreach ($contVariable->contentdata->photogallery as $image)
			<a href="{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $image->url }}" data-lightbox="gallery-item">
				<img class="image_fade" src="{{ env('APP_UPLOAD_PATH_V3') }}thumbnail/{{ $image->url }}" alt="{{ $image->name }}" />
			</a>
		@endforeach
	</div>
</div>