<!-- OK -->
<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">
	<p>
    	<a class="button button-rounded button-reveal button-border linkBtn" href="{!! json_decode($content->variableLang($lang)->content)->content_url !!}" target="{{ (json_decode($content->variableLang($lang)->content)->content_target == 'external') ? '_blank' : '_self' }}">
			<i class="icon-line-link"></i><span>{{ $contVariable->title }}</span>
    	</a>
    </p>	
</div>