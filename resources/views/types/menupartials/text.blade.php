<!-- OK -->
<div class="{{ json_decode($content->variableLang($lang)->props)->props_colvalue }}">
	@php
		$contentArray = explode('<formore></formore>' ,$contVariable->content);
		$contentPro = '';
	@endphp
	@foreach ($contentArray as $arr)
		
		@if ($loop->first && count($contentArray) > 1)
			@php
	        	$contentPro .= "";
	        @endphp
	    @endif

   		@php
        	$contentPro .= $arr;
        @endphp

	    @if ($loop->first && count($contentArray) > 1)
	        @php
	        	$contentPro .= '<div class="collapse" id="collapsePanel'.$contVariable->id.'">';
	        @endphp
	    @endif

	    @if ($loop->last && count($contentArray) > 1)
	        @php
	        	$contentPro .= '
	        	</div><p><a class="button button-rounded button-reveal button-border formorebtn collapsed" data-toggle="collapse" href="#collapsePanel'.$contVariable->id.'" aria-expanded="false" aria-controls="collapsePanel'.$contVariable->id.'"> <i></i><span></span> </a></p>';
	        @endphp
	    @endif

	@endforeach

	{!! $contentPro !!}
		
</div>