@if($content->variable->title != "")
    <div class="col-lg-12">
        <h2 class="header_border">
            @php
                if (empty($content->variableLang($lang))) {
                    print_r($content->variable->title);
                }else{
                    print_r($content->variableLang($lang)->title);
                }
            @endphp
        </h2>
    </div>
@endif

@foreach ($content->subContentThs as $ths)

    @if(!is_null($ths->sub_content_id))

        @php
            $content = $ths->subContent;
            if (empty($content->variableLang($lang))) {
                $contVariable = $content->variable;
            }else{
                $contVariable = $content->variableLang($lang);
            }
        @endphp
        
        @if(json_decode($contVariable->props)->props_section == 'container')
            @if ($content->type == 'text')
                @include('types.menupartials.text')
            @elseif ($content->type == 'photo')
                @include('types.menupartials.photo')
            @elseif ($content->type == 'photogallery')
                @include('types.menupartials.photogallery')
            @elseif ($content->type == 'link')
                @include('types.menupartials.link')
            @elseif ($content->type == 'slide')
                @php $slideData = $content->slide; @endphp
                @include('types.menupartials.slide')
            @elseif ($content->type == 'seperator')
                @include('types.menupartials.seperator')
            @elseif ($content->type == 'form')
                @include('types.menupartials.form')
            @elseif ($content->type == 'mapturkey')
                @include('types.menupartials.mapturkey')
            @elseif ($content->type == 'rssfeed')
                @include('types.menupartials.rssfeed')
            @elseif ($content->type == 'code')
                @include('types.menupartials.code')
            @endif
        @endif

    @else
        
        <!-- Özet Menü Item -->
        @php
            $menuSum = $ths->subMenu;
        @endphp
        @if ($menuSum->type == 'photogallery')
            @include('types.menusummary.photogallery')
        @elseif (starts_with($menuSum->type, 'list'))
            @include('types.menusummary.'.json_decode($ths->props)->props_sum_type)
        @endif

    @endif	

@endforeach





