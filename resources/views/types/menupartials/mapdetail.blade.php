<!-- OK -->

		@php

			if($param == 'bolge'){
				//dump($param.' - '.$param2);
				$districtVariable = App\DistrictVariable::where('slug', $param2)->first();
				$district = $districtVariable->district;
				//dump($district);
				$staffs = App\Staff::where('district_id', $district->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $districtVariable->name;
			}else if($param == 'il'){
				//dump($param.' - '.$param2);
				$cityVariable = App\CityVariable::where('slug', $param2)->first();
				$city = $cityVariable->city;
				//dump($city);
				$staffs = App\Staff::where('city_id', $city->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $cityVariable->name;
			}else if($param == 'ildetay'){
				//dump($param.' - '.$param2);
				$cityVariable = App\CityVariable::where('slug', $param2)->first();
				$city = $cityVariable->city;
				//dump($city);
				$staffs = App\Staff::where('city_id', $city->id)->where('county_id', NULL)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $cityVariable->name;
			}else if($param == 'ilcedetay'){
				//dump($param.' - '.$param2);
				$countyVariable = App\CountyVariable::where('slug', $param2)->first();
				$county = $countyVariable->county;
				//dump($county);
				$staffs = App\Staff::where('county_id', $county->id)->where('status', 'active')->where('deleted', 'no')->orderBy('order')->get();
				//dump($staffs);
				$pageTitle = $countyVariable->name;
			}

		@endphp
	
		<div class="col-lg-12">
			<div class="fancy-title title-dotted-border title-center" style="margin-top: -50px;">
				<h1>{{ $pageTitle }}</h1>
			</div>
		</div>
		

		<div class="col-lg-8">
			
			@include('types.menupartials.mapturkey')
			
			@php
				$__cityid = '';
				$__countyid = '';
			@endphp
			@foreach($staffs as $staff)

				@if($staff->variable->title == 'bolgemuduru')
					@php
						if(empty($staff->variable->photo_url)){
		                	$imgimg = '<img src="'.url(env('APP_UPLOAD_PATH_V3').'default.png').'" />';
		                }else{
		                	$imgimg = '<img src="'.url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$staff->variable->photo_url).'" />';
		                }

						$fixedStaff = '<div class="team team-list team-leader dark clearfix">
			                    <div class="team-image staff-image">'.$imgimg.'</div>
			                    <div class="team-desc">
			                        <div class="team-title">
			                            <h3>'.$staff->variable->name.'</h3>
			                            <span class="grad"> BÖLGE MÜDÜRÜ </span>
			                        </div>
			                        <div class="team-content">
										<p>'.$staff->variable->email.'</p>
										<p>'.$staff->variable->gsm.'</p>
										<p>'.$staff->variable->phone.'</p>
										<p>'.$staff->variable->address.'</p>
			                        </div>
			                    </div>
			                </div>';

		            @endphp

				@else

					@if($staff->city_id != $__cityid || $staff->county_id != $__countyid)
						<div class="fancy-title title-dotted-border" style="margin: 30px 0;">
							@php
								$__cityid = $staff->city_id;
								$__countyid = $staff->county_id;
							@endphp

							<h3>
								{{ $staff->city->variable->name }} 
								@if(!is_null($staff->county_id))
									- {{ $staff->county->variable->name }}
								@endif
							</h3>
						</div>
					@else
						<div class="line" style="margin: 30px 0;"></div>
					@endif

	                <div class="team team-list clearfix">
	                    <div class="team-image staff-image">
	                        @if (empty($staff->variable->photo_url))
	                            <img src="{{ url(env('APP_UPLOAD_PATH_V3').'default.png') }}" />
	                        @else
	                            <img src="{{ url(env('APP_UPLOAD_PATH_V3').'thumbnail/'.$staff->variable->photo_url) }}" />
	                        @endif
	                    </div>
	                    <div class="team-desc">
	                        <div class="team-title">
	                            <h3>{{ $staff->variable->name }}</h3>
	                            <span class="grad">

		                            @if($staff->variable->title == 'bolgemuduru')
		                            	BÖLGE MÜDÜRÜ
		                            @elseif($staff->variable->title == 'bolgekoordinatoru')
										BÖLGE KOORDİNATÖRÜ
		                            @elseif($staff->variable->title == 'acentemuduru')
										ACENTE MÜDÜRÜ
		                            @elseif($staff->variable->title == 'acentetemsilcisi')
		                            	ACENTE TEMSİLCİSİ
		                            @endif

	                            </span>
	                        </div>
	                        <div class="team-content">
	                        	@if(!empty($staff->variable->email))
									<p><strong>E-Posta: {{ $staff->variable->email }}</strong></p>
								@endif
								@if(!empty($staff->variable->gsm))
									<p><strong>GSM:</strong> {{ $staff->variable->gsm }}</p>
								@endif
								@if(!empty($staff->variable->phone))
									<p><strong>Tel:</strong> {{ $staff->variable->phone }}</p>
								@endif
								@if(!empty($staff->variable->address))
									<p><strong>Adres:</strong> {{ $staff->variable->address }}</p>
								@endif

	                        </div>

	                    </div>
	                </div>
		            
		        @endif

			@endforeach

		</div>

		<div class="col-lg-4">

			@if (!empty($fixedStaff))

				{!! $fixedStaff !!}

			@endif

		</div>




		<div class="clear"></div>

