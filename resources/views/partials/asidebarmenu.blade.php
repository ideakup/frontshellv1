@if (empty($submenu))
    @if ($menuitem->subMenuAside->count() > 0)
    	@php $pp++; @endphp
		<li>
            <a href="{{$menuitem->variableLang($lang)->slug}}">
                <div>{{ $menuitem->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' * '.$pp --}}</div>
            </a>
        </li>
        
        @if ($menuitem->subMenuAside->count() > 0)
			<ul>
                @foreach ($menuitem->subMenuAside as $menuitem)
                    @if ($menuitem->position != 'top')
                        @include('partials.asidebarmenu', ['menuitem' => $menuitem, 'submenu' => true])
                    @endif
                @endforeach
            </ul>
        @endif
    @else
        @if($menuitem->position == 'aside' || $menuitem->position == 'all')
            @if (!is_null($menuitem->variableLang($lang)))
                
                @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery')
                    <li>
                        <a href="{{ url($langSlug.'/'.$menuitem->variableLang($lang)->slug) }}">
                            <div>{{ $menuitem->variableLang($lang)->menutitle }}</div>
                        </a>
                    </li>
                @elseif ($menuitem->type == 'link' && !is_null($menuitem->variableLang($lang)->stvalue))
                    <li>
                        <a href="{{ json_decode($menuitem->variableLang($lang)->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang($lang)->stvalue)->target }}">
                            <div>{{ $menuitem->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' + '.$i --}}</div>
                        </a>
                    </li>
                @endif
                
            @endif
        @endif
    @endif
@else
    @if ($menuitem->subMenuAside->count() > 0)
    	@php $pp++; @endphp
		<li>
            <a href="{{$menuitem->variableLang($lang)->slug}}">
                {{ $menuitem->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' ? '.$pp --}}
            </a>
        </li>

        @if ($menuitem->subMenuAside->count() > 0)
			<ul>
                @foreach ($menuitem->subMenuAside as $menuitem)
                    @if ($menuitem->position != 'top')
                        @include('partials.asidebarmenu', ['menuitem' => $menuitem, 'submenu' => true])
                    @endif
                @endforeach
            </ul>
        @endif
    @else
    	@php $pp++; @endphp
        @if($menuitem->position == 'aside' || $menuitem->position == 'all')
            @if (!is_null($menuitem->variableLang($lang)))
        		
                @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery')
                    <li>
                        <a href="{{ url($langSlug.'/'.$menuitem->variableLang($lang)->slug) }}">
                            <div>{{ $menuitem->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' + '.$i --}}</div>
                        </a>
                    </li>
                @elseif ($menuitem->type == 'link' && !is_null($menuitem->variableLang($lang)->stvalue))
                    <li>
                        <a href="{{ json_decode($menuitem->variableLang($lang)->stvalue)->link }}" target="_{{ json_decode($menuitem->variableLang($lang)->stvalue)->target }}">
                            <div>{{ $menuitem->variableLang($lang)->menutitle }} {{-- $menuitem->subMenuAside->count().' + '.$i --}}</div>
                        </a>
                    </li>
                @endif
            @endif
        @endif
    @endif
@endif