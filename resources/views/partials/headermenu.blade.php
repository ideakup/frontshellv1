@if (empty($submenu))
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        @if (is_null($menuitem->top_id) && $menuitem->subMenuTop->count() > 2 && $menuitem->dropdowntype == 'mega01')

            <li class="mega-menu">
                <a href="@if ($menuitem->type == 'menuitem')#@else{{ url($langSlug.'/'.$menuitemVariable->slug) }}@endif">
                    <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' * '.$i --}} </div>
                </a>
                <div class="mega-menu-content style-2 clearfix">
                    <ul class="mega-menu-column col-lg-12">
                        <li>
                            <div class="widget clearfix">
                                
                                <!-- Portfolio Items
                                ============================================= -->
                                <div class="portfolio @if ($menuitem->subMenuTop->count() > 4) portfolio-4 @else portfolio-{{$menuitem->subMenuTop->count()}} @endif grid-container clearfix" data-layout="fitRows" data-transition="0s">
                                    
                                    @foreach ($menuitem->subMenuTop as $megamenuitem)
                                        @php
                                            if (empty($megamenuitem->variableLang($lang))) {
                                                $megamenuitemVariable = $megamenuitem->variable;
                                            }else{
                                                $megamenuitemVariable = $megamenuitem->variableLang($lang);
                                            }
                                        @endphp
                                        <article class="portfolio-item pf-media pf-icons">
                                            <div class="portfolio-image">
                                                @if (!is_null($megamenuitemVariable))
                                                    @if ($megamenuitem->type == 'content' || starts_with($megamenuitem->type, 'list') || $megamenuitem->type == 'photogallery')
                                                        <a href="{{ url($langSlug.'/'.$megamenuitemVariable->slug) }}">
                                                            @if(!empty($megamenuitemVariable->stvalue))
                                                                <img src="{{ env('APP_UPLOAD_PATH_V3') }}small/{{ $megamenuitemVariable->stvalue }}" />
                                                            @endif
                                                            <div class="portfolio-overlay"> </div>
                                                        </a>
                                                    @elseif ($megamenuitem->type == 'link' && !is_null($megamenuitemVariable->stvalue))
                                                        <!--
                                                            <a href="{{ json_decode($megamenuitemVariable->stvalue)->link }}" target="_{{ json_decode($megamenuitemVariable->stvalue)->target }}">
                                                                <img src="images/portfolio/4/1.jpg" />
                                                                <div class="portfolio-overlay"> </div>
                                                            </a>
                                                        -->
                                                    @elseif ($megamenuitem->type == 'menuitem')
                                                        <!--
                                                            <a href="#">
                                                                <img src="images/portfolio/4/1.jpg" />
                                                                <div class="portfolio-overlay"> </div>
                                                            </a>
                                                        -->
                                                    @endif
                                                @endif
                                            </div>
                                            <div class="portfolio-desc">
                                                
                                                @if (!is_null($megamenuitemVariable))
                                                    @if ($megamenuitem->type == 'content' || starts_with($megamenuitem->type, 'list') || $megamenuitem->type == 'photogallery')
                                                        <h3>
                                                            <a href="{{ url($langSlug.'/'.$megamenuitemVariable->slug) }}">
                                                                <div>{{ $megamenuitemVariable->menutitle }}</div>
                                                            </a>
                                                        </h3>
                                                        <span>
                                                            {{$megamenuitem->description}}
                                                        </span>
                                                    @elseif ($megamenuitem->type == 'link' && !is_null($megamenuitemVariable->stvalue))
                                                        <!--
                                                            <h3>
                                                                <a href="{{ json_decode($megamenuitemVariable->stvalue)->link }}" target="_{{ json_decode($megamenuitemVariable->stvalue)->target }}">
                                                                    <div>{{ $megamenuitemVariable->menutitle }} {{-- $megamenuitem->subMenuTop->count().' + '.$i --}}</div>
                                                                </a>
                                                            </h3>
                                                            <span>Frontline harness criteria governance freedom contribution. Campaign Angelina Jolie natural resources, Rockefeller peaceful philanthropy human potential. Justice; outcomes</span>
                                                        -->
                                                    @elseif ($megamenuitem->type == 'menuitem')
                                                        <!--
                                                            <h3>
                                                                <a href="#">
                                                                    <div>{{ $megamenuitemVariable->menutitle }} {{-- $megamenuitem->subMenuTop->count().' + '.$i --}}</div>
                                                                </a>
                                                            </h3>
                                                            <span>Frontline harness criteria governance freedom contribution. Campaign Angelina Jolie natural resources, Rockefeller peaceful philanthropy human potential. Justice; outcomes</span>
                                                        -->
                                                    @endif
                                                @endif
                                                
                                            </div>
                                        </article>
                                    @endforeach

                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
            </li>

        @else
            <li>
                <a href="@if ($menuitem->type == 'menuitem')#@else{{ url($langSlug.'/'.$menuitemVariable->slug) }}@endif">
                    <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' * '.$i --}} </div>
                </a>
                @if ($menuitem->subMenuTop->count() > 0)
                    <ul>
                        @foreach ($menuitem->subMenuTop as $menuitem)
                            @php
                                if (empty($menuitem->variableLang($lang))) {
                                    $menuitemVariable = $menuitem->variable;
                                }else{
                                    $menuitemVariable = $menuitem->variableLang($lang);
                                }
                            @endphp
                            @if ($menuitem->position != 'aside')
                                @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                            @endif
                        @endforeach
                    </ul>
                @endif
            </li>
        @endif
    @else
        @if($menuitem->position == 'top' || $menuitem->position == 'all')
            <li>
                @if (!is_null($menuitemVariable))
                    @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery' || $menuitem->type == 'mixed')
                        <a href="{{ url($langSlug.'/'.$menuitemVariable->slug) }}">
                            <div>{{ $menuitemVariable->menutitle }}</div>
                        </a>
                    @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                        <a href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'menuitem')
                        <a href="#">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @endif
                @endif
            </li>
        @endif
    @endif
@else
    @if ($menuitem->subMenuTop->count() > 0)
        @php $i++; @endphp
        <li>
            <a href="@if ($menuitem->type == 'menuitem')#@else{{ url($langSlug.'/'.$menuitemVariable->slug) }}@endif">
                <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' ? '.$i --}}</div>
            </a>
            @if ($menuitem->subMenuTop->count() > 0)
                <ul>
                    @foreach ($menuitem->subMenuTop as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @if ($menuitem->position != 'aside')
                            @include('partials.headermenu', ['menuitem' => $menuitem, 'submenu' => true])
                        @endif
                    @endforeach
                </ul>
            @endif
        </li>
    @else
        @php $i++; @endphp
        @if($menuitem->position == 'top' || $menuitem->position == 'all')
            @if (!is_null($menuitemVariable))
                <li>
                    @if ($menuitem->type == 'content' || starts_with($menuitem->type, 'list') || $menuitem->type == 'photogallery' || $menuitem->type == 'mixed')
                        <a href="{{ url($langSlug.'/'.$menuitemVariable->slug) }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'link' && !is_null($menuitemVariable->stvalue))
                        <a href="{{ json_decode($menuitemVariable->stvalue)->link }}" target="_{{ json_decode($menuitemVariable->stvalue)->target }}">
                            <div>{{ $menuitemVariable->menutitle }} {{-- $menuitem->subMenuTop->count().' + '.$i --}}</div>
                        </a>
                    @elseif ($menuitem->type == 'menuitem')
                        <a href="#">
                            <div>{{ $menuitemVariable->menutitle }}</div>
                        </a>
                    @endif
                </li>
            @endif
        @endif
    @endif
@endif