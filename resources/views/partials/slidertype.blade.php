@switch($__slidertype)
    
    @case("image")
        
        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-parallax page-title-dark" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $menu->variableLang($lang)->stvalue }}'); background-size: 100%; padding: 120px 0;" data-bottom-top="background-position:0px 0px;" data-top-bottom="background-position:0px 0px;">

            <div class="container clearfix page-image">
                <h2>{{ $menu->variableLang($lang)->name }}</h2>
                
                @include('partials.breadcrumb')
                
            </div>

        </section><!-- #page-title end -->

        @break

    @case("slider")

        @if(is_null($attr) || $content->type != 'mapturkey')


            <section id="slider" class="slider-element slider-parallax swiper_wrapper {{ $wsConfig['sliderClass'] }} clearfix" data-loop="true" data-autoplay="7000" data-speed="650" data-effect="fade" style="height: 600px;">

                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">

                        @foreach ($menu->slider as $sElement)
                            @php
                                if (empty($sElement->variableLang($lang))) {
                                    $elementVariable = $sElement->variable;
                                }else{
                                    $elementVariable = $sElement->variableLang($lang);
                                }

                                $alwal = explode(':',$sElement->align);
                            @endphp
                            @if (!is_null($elementVariable->image_url))
                                
                                <div class="swiper-slide @if($sElement->theme == 'light') dark @endif" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}');">
                                    <div class="container clearfix">
                                        <div class="slider-caption @if($alwal[1] == 'center') slider-caption-center @elseif($alwal[1] == 'right') slider-caption-right @endif @if($alwal[0] == 'top') slider-wcaption-top @elseif($alwal[0] == 'bottom') slider-wcaption-bottom @endif " style="color: #FFF;">

                                            @if (!starts_with($elementVariable->title, '.'))
                                                <h1 data-animate="fadeInUp">{!! $elementVariable->title !!}</h1>
                                                <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">{!! $elementVariable->description !!}</p>
                                            @endif

                                            @if (!empty($elementVariable->button_text))

                                                <a href="{{ $elementVariable->button_url }}" data-animate="fadeInUp" data-delay="400" class="button button-border button-rounded button-fill fill-from-bottom @if($sElement->theme == 'light') button-custom-light @else button-custom-dark @endif">
                                                    <span>{{ $elementVariable->button_text }}</span>
                                                </a>

                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>

                            @endif
                        @endforeach

                    </div>
                    @if($wsConfig['sliderNavVisible'])
                        <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
                        <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
                        <div class="swiper-pagination"></div>
                    @endif
                </div>

            </section>

        @endif

    @break

    @case("full-slider")

        @if(is_null($attr) || $content->type != 'mapturkey')

       
            <section id="slider" class="slider-element swiper_wrapper full-screen clearfix" data-loop="true">


                <div class="swiper-container swiper-parent">
                    <div class="swiper-wrapper">

                        @foreach ($menu->slider as $sElement)
                            @php
                                if (empty($sElement->variableLang($lang))) {
                                    $elementVariable = $sElement->variable;
                                }else{
                                    $elementVariable = $sElement->variableLang($lang);
                                }

                                $alwal = explode(':',$sElement->align);
                            @endphp
                            @if (!is_null($elementVariable->image_url))
                                
                                <div class="swiper-slide @if($sElement->theme == 'light') dark @endif" style="background-image: url('{{ env('APP_UPLOAD_PATH_V3') }}xlarge/{{ $elementVariable->image_url }}');">
                                    <div class="container clearfix">
                                        <div class="slider-caption @if($alwal[1] == 'center') slider-caption-center @elseif($alwal[1] == 'right') slider-caption-right @endif @if($alwal[0] == 'top') slider-wcaption-top @elseif($alwal[0] == 'bottom') slider-wcaption-bottom @endif " style="color: #FFF;">

                                            @if (!starts_with($elementVariable->title, '.'))
                                                <h1 data-animate="fadeInUp">{!! $elementVariable->title !!}</h1>
                                                <p class="d-none d-sm-block" data-animate="fadeInUp" data-delay="200">{!! $elementVariable->description !!}</p>
                                            @endif

                                            @if (!empty($elementVariable->button_text))

                                                <a href="{{ $elementVariable->button_url }}" data-animate="fadeInUp" data-delay="400" class="button button-border button-rounded button-fill fill-from-bottom @if($sElement->theme == 'light') button-custom-light @else button-custom-dark @endif">
                                                    <span>{{ $elementVariable->button_text }}</span>
                                                </a>

                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>

                            @endif
                        @endforeach

                    </div>
                    @if($wsConfig['sliderNavVisible'])
                        <div class="slider-arrow-left"><i class="icon-angle-left"></i></div>
                        <div class="slider-arrow-right"><i class="icon-angle-right"></i></div>
                        <div class="swiper-pagination"></div>
                    @endif
                </div>

            </section>

        @endif

    @break

    @default
        
        @if($wsConfig['pageTitle'])
            <section id="page-title" class="page-title-mini">

                <div class="container clearfix">
                    <h1>{{ $menu->variableLang($lang)->name }}</h1>
                    @include('partials.breadcrumb')
                </div>

            </section>
        @endif
        
@endswitch