<footer id="footer" class="dark">

            <!-- Copyrights
                ============================================= -->
                <div id="copyrights">

                    <div class="container clearfix">

                        <div class="col_half">
                            <div class="row ">
                              <div class="col">
                                  <img style="width: 300px;" src="{{ url('logos/footer-logo-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" class="footer-logo" />
                              </div>
                              <div class="col">
                                <div class="row">
                                 <img  src="{{ url('images/TraceLogo.png') }}" alt="" class="footer-logo" />
                                 <img src="{{ url('images/broker.png') }}" alt="" class="footer-logo" />

                             </div>

                         </div>


                     </div>
                 </div>

                 <div class="col_half col_last tright">
                    <div class="copyrights-menu copyright-links fright clearfix">
                     @foreach ($topmenus as $menuitem)
                     @php
                     if (empty($menuitem->variableLang($lang))) {
                        $menuitemVariable = $menuitem->variable;
                    }else{
                        $menuitemVariable = $menuitem->variableLang($lang);
                    }

                          //  dd($menuitem->status);
                    @endphp

                    @if ($menuitem->position!="none")

                    <a href="{{$menuitemVariable->slug}}">{{$menuitemVariable->menutitle}}</a>

                    @endif
                    @endforeach


                </div>
                <div class="fright clearfix">
                    {{ $sitesettings->where('slug', 'copyright')->first()->value }}

                </div>
            </div>

        </div>

    </div><!-- #copyrights end -->

        </footer><!-- #footer end -->