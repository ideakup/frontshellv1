<footer id="footer" class="dark">

            <!-- Copyrights
                ============================================= -->
                <div id="copyrights" style="background-color: #323a43">

                    <div class="container clearfix">

                        <div class="col_half">
                            <div class="row ">
                              <div class="col">
                                  <img style="width: 200px;" src="{{ url('logos/footer-logo-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" class="footer-logo" />
                              </div>
                              <div class="col">
                                <div class="row">

                                </div>

                            </div>


                        </div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="copyrights-menu copyright-links fright clearfix">
                           @foreach ($topmenus as $menuitem)
                           @php
                           if (empty($menuitem->variableLang($lang))) {
                            $menuitemVariable = $menuitem->variable;
                        }else{
                            $menuitemVariable = $menuitem->variableLang($lang);
                        }

                          //  dd($menuitem->status);
                        @endphp

                        @if ($menuitem->position!="none")

                        <a style="color: #cdba6d" href="{{$menuitemVariable->slug}}">{{$menuitemVariable->menutitle}}</a>

                        @endif
                        @endforeach


                    </div>
                    <div class="fright clearfix">
                     
                       <span id="cp-copy"> {{ $sitesettings->where('slug', 'copyright')->first()->value }}</span> 

                    </div>
                </div>

            </div>

        </div><!-- #copyrights end -->

        </footer><!-- #footer end -->