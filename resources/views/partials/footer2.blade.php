<footer id="footer" class="dark">
    <div class="container">
        <div class="footer-widgets-wrap clearfix">

            <div class="col_full">
                <img src="{{ url('logos/logo-dark-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" class="footer-logo" />
            </div>

            <div class="col_two_third">
                <div class="widget clearfix">
                    <h4> İletişim Bilgileri</h4>
                    @if (!empty($sitesettings->where('slug', 'footer-description')->first()->value))
                        <p>{{ $sitesettings->where('slug', 'footer-description')->first()->value }}</p>
                    @endif

                    <div style="background: url('/images/world-map.png') no-repeat center center; background-size: 100%;">
                        @if (!empty($sitesettings->where('slug', 'footer-address')->first()->value))
                            <address>
                                <abbr title="Adres" style="display: inline-block;"><strong>Adres:</strong></abbr> {{ $sitesettings->where('slug', 'footer-address')->first()->value }}
                            </address>
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-phone')->first()->value))
                            <abbr title="Telefon No"><strong>Telefon No:</strong></abbr> {{ $sitesettings->where('slug', 'footer-phone')->first()->value }}<br>
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-fax')->first()->value))
                            <abbr title="Faks No"><strong>Faks No:</strong></abbr> {{ $sitesettings->where('slug', 'footer-fax')->first()->value }}<br>
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-gsm')->first()->value))
                            <abbr title="GSM No"><strong>GSM No:</strong></abbr> {{ $sitesettings->where('slug', 'footer-gsm')->first()->value }}<br>
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-email')->first()->value))
                            <abbr title="E-posta Adresi"><strong>E-posta:</strong></abbr> {{ $sitesettings->where('slug', 'footer-email')->first()->value }}<br>
                        @endif
                    </div>

                </div>

            </div>


            <div class="col_one_third col_last">

                <div class="widget widget_links clearfix">
                    <h4> Menü </h4>
                    <ul class="footer-menu">
                        @php $i = 0; @endphp
                        @foreach ($topmenus as $menuitem)
                            @php
                                if (empty($menuitem->variableLang($lang))) {
                                    $menuitemVariable = $menuitem->variable;
                                }else{
                                    $menuitemVariable = $menuitem->variableLang($lang);
                                }
                            @endphp
                            @include('partials.footermenu')
                        @endforeach
                    </ul>
                </div>

            </div>

        </div>
    </div>

    <div id="copyrights">
        <div class="container clearfix">
            <div class="col_half">
                {{ $sitesettings->where('slug', 'copyright')->first()->value }}
                <!-- <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div> -->
            </div>

            <div class="col_half col_last tright">
                <div class="fright clearfix">

                    @if (!empty($sitesettings->where('slug', 'social-facebook')->first()->value))
                        <a href="{{ $sitesettings->where('slug', 'social-facebook')->first()->value }}" target="_blank" class="social-icon si-small si-rounded si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-instagram')->first()->value))
                        <a href="{{ $sitesettings->where('slug', 'social-instagram')->first()->value }}" target="_blank" class="social-icon si-small si-rounded si-instagram">
                            <i class="icon-instagram"></i>
                            <i class="icon-instagram"></i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-linkedin')->first()->value))
                        <a href="{{ $sitesettings->where('slug', 'social-linkedin')->first()->value }}" target="_blank" class="social-icon si-small si-rounded si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-twitter')->first()->value))
                        <a href="{{ $sitesettings->where('slug', 'social-twitter')->first()->value }}" target="_blank" class="social-icon si-small si-rounded si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>
                    @endif

                    @if (!empty($sitesettings->where('slug', 'social-youtube')->first()->value))
                        <a href="{{ $sitesettings->where('slug', 'social-youtube')->first()->value }}" target="_blank" class="social-icon si-small si-rounded si-youtube">
                            <i class="icon-youtube"></i>
                            <i class="icon-youtube"></i>
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </div>

</footer>
