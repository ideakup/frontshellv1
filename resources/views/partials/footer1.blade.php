<footer id="footer" style="background-color: #191919" >
    <div class="container">
        <div class="footer-widgets-wrap clearfix">
            <div class="row">
                <div class="col-lg-4">

                    <div class="widget clearfix">
                        <img style="width: 300px" src="{{ url('logos/logo-dark-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" class="footer-logo" />
                    </div>



                </div>



                <div class="col-lg-4">
                    <H3>İletişim Bilgileri</H3>
                    <p>Eti Mahallesi Toros Sokak No:7/24 Çankaya Ankara</p>
                    <p>denizhan@denizhanatalay.av.tr</p>

                </div>
                <div class="col-lg-4 ">
                    <div class="widget clearfix">

                        <div class=" widget_links footer_menu_container ">
                            <H3 >Menü</H3>

                            <ul>
                                @foreach ($topmenus as $topmenu)
                                @php
                                if (empty($topmenu->variableLang($lang))) {
                                    $topmenuVariable = $topmenu->variable;
                                }else{
                                    $topmenuVariable = $topmenu->variableLang($lang);
                                }

                                @endphp

                                @if($topmenu->type != 'menuitem')
                                <li><a href="{{ url($lang.'/'.$topmenuVariable->slug) }}">{{ $topmenuVariable->menutitle }}</a></li>
                                @endif

                                @endforeach
                            </ul>

                        </div>


                    </div>

                </div>



            </div>
        </div><!-- .footer-widgets-wrap end -->

    </div>

    <!-- Copyrights
        ============================================= -->
        <div style="background-color: black" id="copyrights">

            <div class="container clearfix">

                <div class="col_full" style="margin-bottom: 0 !important; text-align: center;">
                   <span id="cp-copy"> {{ $sitesettings->where('slug', 'copyright')->first()->value }}</span> 
               </div>

           </div>

       </div><!-- #copyrights end -->

   </footer><!-- #footer end -->


