@if(config('webshell.siteDefaults.topBar') || $langs->count() > 1)
    <div id="top-bar" class="top-bar-white {{config('webshell.pageProperties.default.topbarClass') }} ">
        <div class="container clearfix">

            <div class="col_half nobottommargin d-none d-md-block">
                @if(config('webshell.siteDefaults.topBar'))
                    <p class="nobottommargin">
                        @if (!empty($sitesettings->where('slug', 'footer-phone')->first()->value))
                            <strong>İletişim:</strong> {{ $sitesettings->where('slug', 'footer-phone')->first()->value }} |
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-gsm')->first()->value))
                            <strong>GSM:</strong> {{ $sitesettings->where('slug', 'footer-gsm')->first()->value }} |
                        @endif

                        @if (!empty($sitesettings->where('slug', 'footer-email')->first()->value))
                            <strong>E-posta:</strong> {{ $sitesettings->where('slug', 'footer-email')->first()->value }}
                        @endif
                    </p>
                @endif
            </div>

            
            <div class="col_half col_last fright nobottommargin">
                @if($langs->count() > 1)
                    <div class="top-links">

                        <ul>

                            @if($langs->count() > 1)
                                @foreach($langs as $lng)
                                    @if(!empty($menu->variableLang($lng->code)->slug))
                                        <li>
                                            <a href="{{ url($lng->code.'/'.$menu->variableLang($lng->code)->slug) }}{{ (empty($content) == false) ? '/'.$content->variableLang($lng->code)->slug : '' }}">
                                                {{ $lng->code }}
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif

                            @if(false)
                                <!--
                                    <li><a href="#">Login</a>
                                        <div class="top-link-section">
                                            <form id="top-login">
                                                <div class="input-group" id="top-login-username">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="icon-user"></i></div>
                                                    </div>
                                                    <input type="email" class="form-control" placeholder="Email address" required="">
                                                </div>
                                                <div class="input-group" id="top-login-password">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-text"><i class="icon-key"></i></div>
                                                    </div>
                                                    <input type="password" class="form-control" placeholder="Password" required="">
                                                </div>
                                                <label class="checkbox">
                                                  <input type="checkbox" value="remember-me"> Remember me
                                                </label>
                                                <button class="btn btn-danger btn-block" type="submit">Sign in</button>
                                            </form>
                                        </div>
                                    </li>
                                -->
                            @endif

                        </ul>
                    </div>
                @endif
            </div>


        </div>
    </div>
@endif

@php
    $headerStyle = '';
    if ($__headertheme == 'light') {
        $headerStyle = 'class="'.$wsConfig['headerClass'].'"';
    } elseif ($__headertheme == 'dark') {
        $headerStyle = 'class="'.$wsConfig['headerClass'].' dark"';
    } elseif ($__headertheme == 'lightdark') {
        $headerStyle = 'class="'.$wsConfig['headerClass'].'" data-sticky-class="dark"';
    } elseif ($__headertheme == 'darklight') {
        $headerStyle = 'class="'.$wsConfig['headerClass'].' dark" data-sticky-class="not-dark"';
    }
    //dump($headerStyle);
@endphp

<header id="header" {!! $headerStyle !!}>
    <div id="header-wrap">
        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <div id="logo">
                <a href="{{ url('/') }}" class="standard-logo" data-dark-logo="{{ url('logos/logo-dark-tr.'.config('webshell.siteDefaults.logoExtension')) }}"><img src="{{ url('logos/logo-light-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" /></a>
                <a href="{{ url('/') }}" class="retina-logo" data-dark-logo="{{ url('logos/logo-dark-tr.'.config('webshell.siteDefaults.logoExtension')) }}"><img src="{{ url('logos/logo-light-tr.'.config('webshell.siteDefaults.logoExtension')) }}" alt="" /></a>
            </div>

            <nav id="primary-menu">
                <ul>
                    
                    @php $i = 0; @endphp
                    @foreach ($topmenus as $menuitem)
                        @php
                            if (empty($menuitem->variableLang($lang))) {
                                $menuitemVariable = $menuitem->variable;
                            }else{
                                $menuitemVariable = $menuitem->variableLang($lang);
                            }
                        @endphp
                        @include('partials.headermenu')
                    @endforeach
                
                </ul>

                @if($wsConfig['bodyClass'] == 'overlay-menu')
                    <a href="#" id="overlay-menu-close" class="d-none d-lg-block"><i class="icon-line-cross"></i></a>
                @endif
            </nav>

        </div>
    </div>
</header>