@if ($menu->asidevisible == 'yes')
					
	<!-- Sidebar
	============================================= -->
	<div class="sidebar nobottommargin col_last clearfix">
		<div class="sidebar-widgets-wrap">

			<div id="shortcodes" class="widget widget_links clearfix">

				<h4 class="highlight-me">Alt Menü</h4>
				<ul>
	            	@php $pp = 0; @endphp
			    	@foreach ($asidemenus as $menuitem)
						@include('partials.asidebarmenu')
		    		@endforeach
	            </ul>

			</div>
			
			@include('partials.asidebartags')

		</div>

	</div><!-- .sidebar end -->

@endif