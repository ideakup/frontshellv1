@php
/*
	$tags = array();
	if (is_null($attr) || $menu->type == 'list-query'){
		foreach ($menu->topHasSub as $ths) {
			foreach ($ths->subContent->tags as $tag) {
				$tags[$tag->id] = $tag;
			}
		}
	} else {
		$content = App\Content::find(App\ContentVariable::where('slug', $attr)->first()->content_id);
		foreach ($content->tags as $tag) {
			$tags[$tag->id] = $tag;
		}

		foreach ($content->subContentThs as $subContent) {
			foreach ($subContent->subContent->tags as $tag) {
				$tags[$tag->id] = $tag;
			}		
		}
	}
@endphp

@if(count($tags) > 0)
	<div class="widget clearfix">
		<h4 class="highlight-me">Etiketler</h4>
		<div class="tagcloud">
			@foreach($tags as $tag)
				@php 
					if (empty($tag->variableLang($lang))) {
	                    $tagVariable = $tag->variable;
	                }else{
	                    $tagVariable = $tag->variableLang($lang);
	                }
				@endphp
				<a href="{{ url('tags/'.$tagVariable->slug) }}">{{ $tagVariable->title }}</a>
			@endforeach
		</div>
	</div>
@endif

@php
*/
@endphp