@if ($__breadcrumbvisible == 'yes')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url($mainmenu->variableLang($lang)->slug) }}">{{ $mainmenu->variableLang($lang)->menutitle }}</a></li>

        @if ($mainmenu->variableLang($lang)->slug != $menu->variableLang($lang)->slug)
            <li class="breadcrumb-item active" aria-current="page">
                <a href="{{ url($menu->variableLang($lang)->slug) }}">{{ $menu->variableLang($lang)->menutitle }}</a>
            </li>
        @endif
    </ol>
@endif