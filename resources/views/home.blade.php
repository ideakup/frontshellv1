@extends('layouts.app')

@section('content')
    <section id="content" style="background-color: {{ $wsConfig['contentBgcolor'] }};">

        <div class="content-wrap">
            
            @if (is_null($attr) || $menu->type == 'list-query')
                
                @if ($menu->type == 'content')
                    @include('types.menu.content')
                @elseif ($menu->type == 'photogallery')
                    @include('types.menu.photogallery')
                @elseif ($menu->type == 'link')
                    @include('types.menu.link')
                @elseif ($menu->type == 'calendar')
                    @include('types.menu.calendar')
                @elseif (starts_with($menu->type, 'list'))
                    @include('types.menu.'.$menu->type)
                @endif
                
            @else

                @php 
                    $content = App\Content::find(App\ContentVariable::where('slug', $attr)->first()->content_id); 
                    if (empty($content->variableLang($lang))) {
                        $contVariable = $content->variable;
                    }else{
                        $contVariable = $content->variableLang($lang);
                    }
                @endphp
                
                @if ($menu->asidevisible == 'yes')
                    <div class="container clearfix">
                        <div class="postcontent nobottommargin clearfix">
                @endif
                
                    <div class="section {{ 'light'/*$_props_colortheme*/ }} notopmargin nobottommargin">
                        <div class="{{ $wsConfig['containerClass'] }} clearfix">
                            <div class="row">
                                
                                @if ($content->type == 'group')
                                    @include('types.menupartials.group-'.$menu->type)
                                @elseif ($content->type == 'photogallery')
                                    @include('types.menupartials.photogallery')
                                @elseif ($content->type == 'mapturkey')
                                    @include('types.menupartials.mapdetail')
                                @endif

                            </div>
                        </div>
                    </div>
                    
                @if ($menu->asidevisible == 'yes')
                            </div>
                        @include('partials.asidebar')
                    </div>
                @endif

            @endif

        </div>

    </section>
        
@endsection

@section('inline-scripts')

    @if (is_null($attr) || $menu->type == 'list-query')
        @foreach ($menu->topHasSub as $ths)

            @if(!is_null($ths->sub_content_id))
                @php
                    $element = $ths->subContent;
                @endphp
                @if(!is_null($element))
                    @if ($element->type == 'form')
                        @include('types.menupartials.formjs')
                    @endif

                    @if ($element->type == 'mapturkey')
                        @include('types.menupartials.mapturkeyjs')
                    @endif
                @endif
            @endif

        @endforeach
    @else
        @foreach ($content->subContentThs as $ths)
            @php
                $element = $ths->subContent;
            @endphp
            @if(!is_null($element))
                @if ($element->type == 'form')
                    @include('types.menupartials.formjs')
                @endif

                @if ($element->type == 'mapturkey')
                    @include('types.menupartials.mapturkeyjs')
                @endif
            @endif
        @endforeach
    @endif
    
    <script>

        var map;

        function initMap() {
            if(typeof uluru != 'undefined'){
                map = new google.maps.Map(document.getElementById('map'), {
                    center: uluru,
                    zoom: 16,
                    styles: googleMapStyles
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    icon: '{{ asset('cimages/marker.png') }}',
                    map: map
                });
            }
        }
        
    </script>

@endsection